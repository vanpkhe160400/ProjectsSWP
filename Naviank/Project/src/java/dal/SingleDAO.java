/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import model.*;

/**
 *
 * @author Naviank
 */
public class SingleDAO extends DBContext {

    public User getAccount(String username, String pass) {
        String sql = "select * from Users where username=? and password=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, pass);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new User(rs.getInt("U_ID"),
                        username,
                        pass,
                        rs.getString("U_FirstName"),
                        rs.getString("U_LastName"),
                        rs.getString("U_Address"),
                        rs.getString("U_Phone"),
                        rs.getString("U_Email"),
                        rs.getBoolean("IsAdmin"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public User getAccountByName(String username) {
        String sql = "select * from Users where username=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new User(rs.getInt("U_ID"),
                        username,
                        rs.getString("password"),
                        rs.getString("U_FirstName"),
                        rs.getString("U_LastName"),
                        rs.getString("U_Address"),
                        rs.getString("U_Phone"),
                        rs.getString("U_Email"),
                        rs.getBoolean("IsAdmin"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public User getUserByID(int id) {
        String sql = "select * from Users where U_ID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new User(rs.getInt("U_ID"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("U_FirstName"),
                        rs.getString("U_LastName"),
                        rs.getString("U_Address"),
                        rs.getString("U_Phone"),
                        rs.getString("U_Email"),
                        rs.getBoolean("IsAdmin"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public boolean checkExistedAccount(String username) {
        String sql = "select * from Users where username=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }

    public int insertUser(User c) {
        int res = 0;
        String sql = "INSERT INTO [dbo].[Users]\n"
                + "     VALUES (?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, c.getUsername());
            st.setString(2, c.getPassword());
            st.setString(3, c.getFirstName());
            st.setString(4, c.getLastName());
            st.setString(5, c.getAddress());
            st.setString(6, c.getPhone());
            st.setString(7, c.getEmail());
            st.setBoolean(8, c.isIsAdmin());
            res = st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return res;
    }

    public int updateUser(User c) {
        int res = 0;
        String sql = "UPDATE [dbo].[Users]\n"
                + "   SET [username] = ?\n"
                + "      ,[password] = ?\n"
                + "      ,[U_FirstName] = ?\n"
                + "      ,[U_LastName] = ?\n"
                + "      ,[U_Address] = ?\n"
                + "      ,[U_Phone] = ?\n"
                + "      ,[U_Email] = ?\n"
                + " WHERE U_ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, c.getUsername());
            st.setString(2, c.getPassword());
            st.setString(3, c.getFirstName());
            st.setString(4, c.getLastName());
            st.setString(5, c.getAddress());
            st.setString(6, c.getPhone());
            st.setString(7, c.getEmail());
            st.setInt(8, c.getId());
            res = st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return res;
    }

    public int countUser() {
        String sql = "select count(U_ID)\n"
                + "from Users";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int res = rs.getInt(1);
                return res;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public Category getCateByID(int id) {
        String sql = "select * from Categories where Cat_ID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Category(id, rs.getString("Cat_Name"), rs.getString("Cat_JName"), this.getChefByID(rs.getInt("Chef_ID")));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public int insertCate(Category c) {
        String sql = "INSERT INTO Categories \n"
                + "VALUES\n"
                + "(?, ?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, c.getName());
            st.setString(2, c.getjName());
            st.setInt(3, c.getChef().getId());
            return st.executeUpdate();
        } catch (SQLException e) {
            System.out.println();
        }
        return 0;
    }

    public int updateCate(Category c) {
        String sql = "update Categories\n"
                + "set Cat_Name = ?, Cat_JName = ?, Chef_ID = ?\n"
                + "where Cat_ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, c.getName());
            st.setString(2, c.getjName());
            st.setInt(3, c.getChef().getId());
            st.setInt(4, c.getId());
            return st.executeUpdate();
        } catch (SQLException e) {
            System.out.println();
        }
        return 0;
    }

    public int deleteCate(int id) {
        String sql = "DELETE FROM Categories WHERE Cat_ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            return st.executeUpdate();
        } catch (SQLException e) {
            System.out.println();
        }
        return 0;
    }

    public Dish getDishByID(int id) {
        String sql = "select * from Dishes where Dish_ID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Dish(id,
                        this.getCateByID(rs.getInt("Cat_ID")),
                        rs.getString("Dish_Name"),
                        rs.getString("Dish_Image"),
                        rs.getFloat("Dish_Price"),
                        rs.getString("Dish_Describe"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public int insertDish(Dish d) {
        String sql = "INSERT INTO Dishes \n"
                + "VALUES\n"
                + "(?, ?, ?, ?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, d.getCat().getId());
            st.setString(2, d.getName());
            st.setString(3, d.getImg());
            st.setFloat(4, d.getPrice());
            st.setString(5, d.getDescribe());
            return st.executeUpdate();
        } catch (SQLException e) {
            System.out.println();
        }
        return 0;
    }

    public int updateDish(Dish d) {
        String sql = "UPDATE [dbo].[Dishes]\n"
                + "   SET [Cat_ID] = ?\n"
                + "      ,[Dish_Name] = ?\n"
                + "      ,[Dish_Image] = ?\n"
                + "      ,[Dish_Price] = ?\n"
                + "      ,[Dish_Describe] = ?\n"
                + " WHERE Dish_ID = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, d.getCat().getId());
            st.setString(2, d.getName());
            st.setString(3, d.getImg());
            st.setFloat(4, d.getPrice());
            st.setString(5, d.getDescribe());
            st.setInt(6, d.getId());
            return st.executeUpdate();
        } catch (SQLException e) {
            System.out.println();
        }
        return 0;
    }

    public int deleteDish(int id) {
        String sql = "DELETE FROM Dishes WHERE Dish_ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            return st.executeUpdate();
        } catch (SQLException e) {
            System.out.println();
        }
        return 0;
    }

    public Chef getChefByID(int id) {
        String sql = "select * from Chefs where Chef_ID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Chef(id, rs.getString("Chef_Name"), rs.getString("Chef_Certi"), rs.getString("Chef_Img"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public int insertStaff(String obj, Object o) {
        String sql;
        try {
            if (obj.equals("chef")) {
                sql = "insert into Chefs values(?,?,?)";
                Chef c = (Chef) o;
                PreparedStatement st = connection.prepareStatement(sql);
                st.setString(1, c.getName());
                st.setString(2, c.getCerti());
                st.setString(3, c.getImg());
                return st.executeUpdate();
            } else {
                sql = "insert into Shippers values (?,?,?)";
                Shipper c = (Shipper) o;
                PreparedStatement st = connection.prepareStatement(sql);
                st.setString(1, c.getName());
                st.setString(2, c.getPhone());
                st.setString(3, c.getImg());
                return st.executeUpdate();
            }
        } catch (SQLException e) {
            System.out.println("e");
        }
        return 0;
    }

    public void deleteStaff(String obj, int id) {
        String sql;
        if (obj.equals("chef")) {
            sql = "delete from Chefs where Chef_ID = ?";
        } else {
            sql = "delete from Shippers where Shipper_ID = ?";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    /*
    	Order_ID int PRIMARY KEY IDENTITY(1,1),
	U_ID int not null FOREIGN KEY REFERENCES Customers(U_ID),
	OrderDate date not null,
	OrderTime time,
	ShipAddress nvarchar(255),
	Shipper_ID int not null FOREIGN KEY REFERENCES Shippers (Shipper_ID),
	Total_money float not null
     */
    public Shipper getShipperByID(int id) {
        String sql = "select * from Shippers where Shipper_ID=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Shipper(id, rs.getString("Shipper_Name"), rs.getString("Shipper_Phone"), rs.getString("Shipper_Img"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public Shipper getRandomShipper() {
        MultiDAO dao = new MultiDAO();
        Random rd = new Random();
        List<Shipper> list = dao.getAllShipper();
        Shipper p = list.get(rd.nextInt(list.size()));
        return p;
    }

    public void addOrder(User c, Cart cart, Shipper s) {
        LocalDate curDate = java.time.LocalDate.now();
        LocalTime curTime = java.time.LocalTime.now();
        try {
            String sql = "insert into [Orders] values (?,?,?,?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, c.getId());
            st.setString(2, curDate.toString());
            st.setString(3, curTime.toString());
            st.setString(4, c.getAddress());
            st.setString(5, c.getAddress());
            st.setString(6, c.getAddress());
            st.executeUpdate();
            //
            String sql1 = "select top 1 Order_ID from Orders order by id desc";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            ResultSet rs = st1.executeQuery();
            if (rs.next()) {
                int oid = rs.getInt(1);
                for (Item i : cart.getItems()) {
                    String sql2 = "insert into [OrderDetails] values(?,?,?)";
                    PreparedStatement st2 = connection.prepareStatement(sql2);
                    st2.setInt(1, oid);
                    st2.setInt(2, i.getDish().getId());
                    st2.setInt(3, i.getQuantity());
                    st2.executeUpdate();
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public Order getOrderByID(int id) {
        String sql = "select * from Orders where Order_ID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Order o = new Order(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getBoolean(5),
                        rs.getString(6),
                        rs.getInt(7));
                System.out.println(rs.getFloat(8));
                return o;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void addOrder(Order o) {
        LocalDate curDate = java.time.LocalDate.now();
        LocalTime curTime = java.time.LocalTime.now();
        try {
            String sql = "insert into [Orders] values (?,?,?,?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, o.getCus_id());
            st.setString(2, curDate.toString());
            st.setString(3, curTime.toString());
            st.setBoolean(4, false);
            st.setString(5, o.getAddress());
            st.setInt(6, o.getShip_id());
            st.executeUpdate();
            //
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public Order mostValueOrder() {
        String sql = "select b.*, o.U_ID\n"
                + "from Orders o, Users u,\n"
                + "(\n"
                + "	select  i.Order_ID, sum (d.Dish_Price *  i.Quantity) as Total\n"
                + "	from Dishes d, OrderDetails i\n"
                + "	where i.Dish_ID = d.Dish_ID\n"
                + "	group by i.Order_ID\n"
                + ") as b\n"
                + "where b.Order_ID = o.Order_ID and u.U_ID = o.U_ID  and b.Total =\n"
                + "(	\n"
                + "	select max (c.total) \n"
                + "	from\n"
                + "	(\n"
                + "		select  i.Order_ID, sum (d.Dish_Price *  i.Quantity) as Total\n"
                + "		from Dishes d, OrderDetails i\n"
                + "		where i.Dish_ID = d.Dish_ID\n"
                + "		group by i.Order_ID\n"
                + "	) as c\n"
                + ")";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return this.getOrderByID(rs.getInt(1));
            }
        } catch(SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public float resTotalSales() {
        String sql = "select sum(b.total) as 'Sales'\n"
                + "from \n"
                + "(\n"
                + "	select  i.Order_ID, sum (d.Dish_Price *  i.Quantity) as Total\n"
                + "	from Dishes d, OrderDetails i\n"
                + "	where i.Dish_ID = d.Dish_ID\n"
                + "	group by i.Order_ID\n"
                + ") as b\n";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getFloat(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public float resSales(int month, int year) {
        String sql = "select sum(b.total) as 'Sales'\n"
                + "from Orders o,\n"
                + "(\n"
                + "	select  i.Order_ID, sum (d.Dish_Price *  i.Quantity) as Total\n"
                + "	from Dishes d, OrderDetails i\n"
                + "	where i.Dish_ID = d.Dish_ID\n"
                + "	group by i.Order_ID\n"
                + ") as b\n"
                + "where o.Order_ID = b.Order_ID and MONTH(o.OrderDate) = ? and year(o.orderdate) = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, month);
            st.setInt(2, year);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getFloat(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public static void main(String[] args) {
        SingleDAO dao = new SingleDAO();
        HashMap<Integer, Float> map = new HashMap<>();
        for (int i = 1; i <= 12; i++) {
            map.put(i, dao.resSales(i, 2023));
        }
        map.put(13, dao.resTotalSales());
        for (Integer i : map.keySet()) {
            System.out.println(map.get(i));
        }
    }
}
