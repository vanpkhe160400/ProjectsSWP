/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.*;

/**
 *
 * @author Naviank
 */
public class MultiDAO extends DBContext {

    public List<User> getAllUser() {
        List<User> list = new ArrayList<>();
        String sql = "select * from Users";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new User(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getBoolean(9)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Category> getAllCate() {
        List<Category> list = new ArrayList<>();
        SingleDAO dao = new SingleDAO();
        String sql = "select * from Categories";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Category(rs.getInt("Cat_ID"),
                        rs.getString("Cat_Name"),
                        rs.getString("Cat_JName"),
                        dao.getChefByID(rs.getInt("Chef_ID"))));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Dish> getAllDish() {
        List<Dish> list = new ArrayList<>();
        SingleDAO dao = new SingleDAO();
        String sql = "select * from Dishes";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Dish(rs.getInt("Dish_ID"),
                        dao.getCateByID(rs.getInt("Cat_ID")),
                        rs.getString("Dish_Name"),
                        rs.getString("Dish_Image"),
                        rs.getFloat("Dish_Price"),
                        rs.getString("Dish_Describe")));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Dish> mostPopularDish(int num) {
        List<Dish> list = new ArrayList<>();
        if (num == 0) {
            num = 1000;
        }
        int count = 0;
        SingleDAO dao = new SingleDAO();
        String sql = "select\n"
                + "sum(Quantity) as 'Total', o.Dish_ID\n"
                + "from OrderDetails o join Dishes d\n"
                + "on o.Dish_ID = d.Dish_ID\n"
                + "group by o.Dish_ID\n"
                + "order by Total desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
//            if (num != 0) {
//            } else {
//                st.setString(1, "top "+ 100);
//            }
            ResultSet rs = st.executeQuery();
            while (rs.next() && count < num) {
                list.add(dao.getDishByID(rs.getInt("Dish_ID")));
                ++count;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Shipper> getAllShipper() {
        List<Shipper> list = new ArrayList<>();
        String sql = "select * from Shippers";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Shipper(rs.getInt("Shipper_ID"),
                        rs.getString("Shipper_Name"),
                        rs.getString("Shipper_Phone"),
                        rs.getString("Shipper_Img")));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Chef> getAllChef() {
        List<Chef> list = new ArrayList<>();
        String sql = "select * from Chefs";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Chef(rs.getInt("Chef_ID"),
                        rs.getString("Chef_Name"),
                        rs.getString("Chef_Certi"),
                        rs.getString("Chef_Img")));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Dish> listDishPriceAsc(boolean b) {
        List<Dish> list = new ArrayList<>();
        SingleDAO dao = new SingleDAO();
        String sql;
        if (b) {
            sql = "select * from Dishes\n"
                    + "order by Dish_Price asc";
        } else {
            sql = "select * from Dishes\n"
                    + "order by Dish_Price desc";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Dish(rs.getInt("Dish_ID"),
                        dao.getCateByID(rs.getInt("Cat_ID")),
                        rs.getString("Dish_Name"),
                        rs.getString("Dish_Image"),
                        rs.getFloat("Dish_Price"),
                        rs.getString("Dish_Describe")));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Dish> listDishNameAsc(boolean b) {
        List<Dish> list = new ArrayList<>();
        SingleDAO dao = new SingleDAO();
        String sql;
        if (b) {
            sql = "select * from Dishes\n"
                    + "order by Dish_Name asc";
        } else {
            sql = "select * from Dishes\n"
                    + "order by Dish_Name desc";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Dish(rs.getInt("Dish_ID"),
                        dao.getCateByID(rs.getInt("Cat_ID")),
                        rs.getString("Dish_Name"),
                        rs.getString("Dish_Image"),
                        rs.getFloat("Dish_Price"),
                        rs.getString("Dish_Describe")));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Dish> listDishBelowPrice(boolean b, float price) {
        List<Dish> list = new ArrayList<>();
        SingleDAO dao = new SingleDAO();
        String sql;
        if (b) {
            sql = "select * from Dishes where Dish_Price <= ? order by Dish_Price asc ";
        } else {
            sql = "select * from Dishes where Dish_Price <= ? order by Dish_Price desc ";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setFloat(1, price);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Dish(rs.getInt("Dish_ID"),
                        dao.getCateByID(rs.getInt("Cat_ID")),
                        rs.getString("Dish_Name"),
                        rs.getString("Dish_Image"),
                        rs.getFloat("Dish_Price"),
                        rs.getString("Dish_Describe")));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Dish> listDishOfChef(List<Chef> listC) {
        List<Dish> list = new ArrayList<>();
        SingleDAO dao = new SingleDAO();
        String sql = "select d.Dish_ID, d.Cat_ID, d.Dish_Name, c.Cat_ID, ch.Chef_ID\n"
                + "from Dishes d, Categories c, Chefs ch\n"
                + "where d.Cat_ID = c.Cat_ID and c.Chef_ID = ch.Chef_ID and ch.Chef_ID = ?\n"
                + "order by ch.Chef_ID asc";
        try {
            for (Chef ch : listC) {
                PreparedStatement st = connection.prepareStatement(sql);
                st.setInt(1, ch.getId());
                ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    list.add(dao.getDishByID(rs.getInt("Dish_ID")));
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<List<Dish>> listDishOfChefs(List<Chef> listC) {
        List<List<Dish>> listDish = new ArrayList<>();
        SingleDAO dao = new SingleDAO();
        String sql = "select d.Dish_ID, d.Cat_ID, d.Dish_Name, c.Cat_ID, ch.Chef_ID\n"
                + "from Dishes d, Categories c, Chefs ch\n"
                + "where d.Cat_ID = c.Cat_ID and c.Chef_ID = ch.Chef_ID and ch.Chef_ID = ?\n"
                + "order by ch.Chef_ID asc";
        try {
            for (Chef ch : listC) {
                List<Dish> list = new ArrayList<>();
                PreparedStatement st = connection.prepareStatement(sql);
                st.setInt(1, ch.getId());
                ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    list.add(dao.getDishByID(rs.getInt("Dish_ID")));
                }
                System.out.println("SIZE: " + list.size());
                listDish.add(list);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return listDish;
    }

    public List<Chef> getChefByName(String name) {
        String sql = "select * from Chefs where Chef_Name like ?";
        List<Chef> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + name + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Chef(rs.getInt("Chef_ID"),
                        rs.getString("Chef_Name"),
                        rs.getString("Chef_Certi"),
                        rs.getString("Chef_Img")));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Category> searchCatByName(String name) {
        List<Category> list = new ArrayList<>();
        SingleDAO dao = new SingleDAO();
        String sql = "select * from Categories where Cat_JName like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + name + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Category(rs.getInt("Cat_ID"),
                        rs.getString("Cat_Name"),
                        rs.getString("Cat_JName"),
                        dao.getChefByID(rs.getInt("Chef_ID"))));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Dish> searchDishByName(String name) {
        List<Dish> list = new ArrayList<>();
        SingleDAO dao = new SingleDAO();
        String sql = "select * from Dishes where Dish_Name like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + name + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(dao.getDishByID(rs.getInt("Dish_ID")));
                /*new Dish(rs.getInt("Dish_ID"),
                        dao.getCateByID(rs.getInt("Cat_ID")),
                        rs.getString("Dish_Name"),
                        rs.getString("Dish_Image"),
                        rs.getFloat("Dish_Price"),
                        rs.getString("Dish_Describe"))*/
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Order> getOrdersByDate(String from, String to) {
        List<Order> list = new ArrayList<>();
        String sql = "select * \n"
                + "from Orders o\n"
                + "where o.OrderDate between  cast (? as date) and cast (? as DATE)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, from);
            st.setString(2, to);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Order(rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getBoolean(5),
                        rs.getString(6),
                        rs.getInt(7)));
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    public List<Table> getFreeTable() {
        List<Table> list = new ArrayList<>();
        String sql = "select * from BTables where OnBook = 0";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Table(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        false));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<User> spendMostUser(String from, String to) {
        SingleDAO dao = new SingleDAO();
        String sql = "select o.U_ID, sum(b.Total) as Spent, count(*) as NumOfOrders\n"
                + "from Orders o, Users u,\n"
                + "(\n"
                + "	select  i.Order_ID, sum (d.Dish_Price *  i.Quantity) as Total\n"
                + "	from Dishes d, OrderDetails i\n"
                + "	where i.Dish_ID = d.Dish_ID\n"
                + "	group by i.Order_ID\n"
                + ") as b\n"
                + "where b.Order_ID = o.Order_ID and u.U_ID = o.U_ID\n"
                + "and o.OrderDate between  cast (? as date) and cast (? as DATE)\n"
                + "group by (o.U_ID)\n"
                + "order by Spent desc";
        List<User> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, from);
            st.setString(2, to);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User tmp = dao.getUserByID(rs.getInt(1));
                tmp.setNumofo(rs.getInt(3));
                tmp.setSpent(rs.getFloat(2));
                list.add(tmp);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<String> maxSalesDishName() {
        List<String> list = new ArrayList<>();
        SingleDAO dao = new SingleDAO();
        String sql = "select o.Dish_ID, (sum(Quantity) * d.Dish_Price) as 'Total', sum(quantity) as Quantity\n"
                + "from OrderDetails o, Dishes d\n"
                + "where o.Dish_ID = d.Dish_ID\n"
                + "group by o.Dish_ID, d.Dish_Price\n"
                + "order by total desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Dish tmp = (dao.getDishByID(rs.getInt(1)));
                list.add(tmp.getName());
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Dish> maxSalesDish() {
        List<Dish> list = new ArrayList<>();
        SingleDAO dao = new SingleDAO();
        String sql = "select o.Dish_ID, (sum(Quantity) * d.Dish_Price) as 'Total', sum(quantity) as Quantity\n"
                + "from OrderDetails o, Dishes d\n"
                + "where o.Dish_ID = d.Dish_ID\n"
                + "group by o.Dish_ID, d.Dish_Price\n"
                + "order by total desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Dish tmp = (dao.getDishByID(rs.getInt(1)));
                tmp.setOnorder(rs.getInt(3));
                tmp.setSales(rs.getFloat(2));
                list.add(tmp);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public HashMap<Dish, Float> maxSalesMap() {
        HashMap<Dish, Float> map = new HashMap<>();
        SingleDAO dao = new SingleDAO();
        String sql = "select o.Dish_ID, (sum(Quantity) * d.Dish_Price) as 'Total', sum(quantity) as Quantity\n"
                + "from OrderDetails o, Dishes d\n"
                + "where o.Dish_ID = d.Dish_ID\n"
                + "group by o.Dish_ID, d.Dish_Price\n"
                + "order by total desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                map.put(dao.getDishByID(rs.getInt(1)), rs.getFloat(2));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return map;
    }

    public HashMap<Integer, Integer> getOrderOfDay(int from, int to) {
        HashMap<Integer, Integer> map = new HashMap<>();
        String sql = "select count(*) as Num, DAY(o.OrderDate) as date\n"
                + "from Orders o\n"
                + "where DAY(o.OrderDate) between ? and ?\n"
                + "group by DAY(o.OrderDate)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, from);
            st.setInt(2, to);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                map.put(rs.getInt(2), rs.getInt(1));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return map;
    }

    public static void main(String[] args) {
        MultiDAO dao = new MultiDAO();
        SingleDAO dao2 = new SingleDAO();
        List<Order> list = dao.getOrdersByDate("2023-03-10", "2023-03-11");
        for (Order o : list) {
            System.out.println(o.getId());
        }
    }
}
