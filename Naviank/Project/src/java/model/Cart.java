/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Naviank
 */
public class Cart {

    private List<Item> items;

    public Cart() {
        items = new ArrayList<>();
    }

    public Cart(String txt, List<Dish> list) {
        items = new ArrayList<>();
        if (txt != null && txt.length() != 0) {
            String[] sa = txt.split(",");
            for (String s : sa) {
                String[] n = s.split(":");
                int id = Integer.parseInt(n[0]);
                Dish d;
                d = getDishInCart(list, id);
                Item it = new Item(d, Integer.parseInt(n[1]), d.getPrice());
                if (getItemById(it.getDish().getId()) != null) {
                    Item titem = getItemById(it.getDish().getId());
                    titem.setQuantity(titem.getQuantity() + it.getQuantity());;
                } else {
                    items.add(it);
                }
            }
        }
    }
    
    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
    

    public int getQuantityById(int id) {
        return getItemById(id).getQuantity();
    }

    private Item getItemById(int id) {
        for (Item i : items) {
            if (i.getDish().getId() == id) {
                return i;
            }
        }
        return null;
    }

    public void addItem(Item t) {
        if (getItemById(t.getDish().getId()) != null) {
            Item m = getItemById(t.getDish().getId());
            m.setQuantity(m.getQuantity() + t.getQuantity());
        } else {
            items.add(t);
        }
    }

    public void removeItem(int id) {
        if (getItemById(id) != null) {
            items.remove(getItemById(id));
        }
    }

    public double getTotalMoney() {
        double t = 0;
        for (Item i : items) {
            t += (i.getQuantity() * i.getPrice());
        }
        return t;
    }
    
    private Dish getDishInCart(List<Dish> list, int id) {
        for (Dish d : list) {
            if (d.getId() == id) {
                return d;
            }
        }
        return null;
    }
}
