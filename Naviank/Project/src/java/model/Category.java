/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Naviank
 */
public class Category {

    private int id;
    private String name;
    private String jName;
    private Chef chef;

    public Category() {
    }

    public Category(int id, String name, String jName, Chef chef) {
        this.id = id;
        this.name = name;
        this.jName = jName;
        this.chef = chef;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getjName() {
        return jName;
    }

    public void setjName(String jName) {
        this.jName = jName;
    }

    public Chef getChef() {
        return chef;
    }

    public void setChef(Chef chef) {
        this.chef = chef;
    }

    @Override
    public String toString() {
        return "Category{" + "id=" + id + ", name=" + name + ", jName=" + jName + ", chef=" + chef + '}';
    }

}
