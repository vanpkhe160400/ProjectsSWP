/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Naviank
 */
public class OrderDetail {
    private int ord_id;
    private int dish_id;
    private int quantity;

    public OrderDetail() {
    }

    public OrderDetail(int ord_id, int dish_id, int quantity) {
        this.ord_id = ord_id;
        this.dish_id = dish_id;
        this.quantity = quantity;
    }

    public int getOrd_id() {
        return ord_id;
    }

    public void setOrd_id(int ord_id) {
        this.ord_id = ord_id;
    }

    public int getDish_id() {
        return dish_id;
    }

    public void setDish_id(int dish_id) {
        this.dish_id = dish_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
}
