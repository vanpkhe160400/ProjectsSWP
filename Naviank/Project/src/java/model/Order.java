/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Naviank
 */
public class Order {

    private int id;
    private int cus_id;
    private String date;
    private String time;
    private boolean shipped;
    private String shiptime;
    private String address;
    private int ship_id;
    private float total;

    public Order() {
    }

    public Order(int id, int cus_id, String date, String time, boolean shipped, String shiptime, String address, int ship_id, float total) {
        this.id = id;
        this.cus_id = cus_id;
        this.date = date;
        this.time = time;
        this.shipped = shipped;
        this.shiptime = shiptime;
        this.address = address;
        this.ship_id = ship_id;
        this.total = total;
    }

    public Order(int id, int cus_id, String date, String time, boolean shipped, String address, int ship_id) {
        this.id = id;
        this.cus_id = cus_id;
        this.date = date;
        this.time = time;
        this.shipped = shipped;
        this.address = address;
        this.ship_id = ship_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCus_id() {
        return cus_id;
    }

    public void setCus_id(int cus_id) {
        this.cus_id = cus_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getShiptime() {
        return shiptime;
    }

    public void setShiptime(String shiptime) {
        this.shiptime = shiptime;
    }

    public boolean isShipped() {
        return shipped;
    }

    public void setShipped(boolean shipped) {
        this.shipped = shipped;
    }

    public int getShip_id() {
        return ship_id;
    }

    public void setShip_id(int ship_id) {
        this.ship_id = ship_id;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", cus_id=" + cus_id + ", date=" + date + ", time=" + time + ", shiptime=" + shiptime + ", address=" + address + ", ship_id=" + ship_id + '}';
    }

}
