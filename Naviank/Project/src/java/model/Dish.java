/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Naviank
 */
public class Dish {

    private int id;
    private Category cat;
    private String name;
    private String img;
    private float price;
    private String describe;
    private float sales;
    private int onorder;
            
    public Dish() {
    }

    public Dish(int id, Category cat, String name, String img, float price, String describe) {
        this.id = id;
        this.cat= cat;
        this.name = name;
        this.img = img;
        this.price = price;
        this.describe = describe;
    }

    public Dish(int id, Category cat, String name, String img, float price, String describe, float sales, int onorder) {
        this.id = id;
        this.cat = cat;
        this.name = name;
        this.img = img;
        this.price = price;
        this.describe = describe;
        this.sales = sales;
        this.onorder = onorder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Category getCat() {
        return cat;
    }

    public void setCat(Category cat) {
        this.cat = cat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public float getSales() {
        return sales;
    }

    public void setSales(float sales) {
        this.sales = sales;
    }

    public int getOnorder() {
        return onorder;
    }

    public void setOnorder(int onorder) {
        this.onorder = onorder;
    }

    @Override
    public String toString() {
        return "Dish{" + "id=" + id + ", cat=" + cat + ", name=" + name + ", img=" + img + ", price=" + price + ", describe=" + describe + '}';
    }

}
