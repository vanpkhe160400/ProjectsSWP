/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Naviank
 */
public class Chef {

    private int id;
    private String name;
    private String certi;
    private String img;

    public Chef() {
    }

    public Chef(int id, String name, String certi) {
        this.id = id;
        this.name = name;
        this.certi = certi;
    }

    public Chef(int id, String name, String certi, String img) {
        this.id = id;
        this.name = name;
        this.certi = certi;
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCerti() {
        return certi;
    }

    public void setCerti(String certi) {
        this.certi = certi;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "Chef{" + "id=" + id + ", name=" + name + ", certi=" + certi + ", img=" + img + '}';
    }

}
