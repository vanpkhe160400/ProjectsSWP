/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SingleDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Naviank
 */
public class ManagerSales extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManagerSales</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManagerSales at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SingleDAO d = new SingleDAO();
        request.setAttribute("year", 2023);
        request.setAttribute("month", 0);

        HashMap<Integer, Float> map = new HashMap<>();

        request.getRequestDispatcher("manage_4.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String year_raw = request.getParameter("year");
        if (year_raw == null || year_raw.length() == 0) {
            year_raw = "2023";
        }
        String month_raw = request.getParameter("month");
        int year = Integer.parseInt(year_raw);
        int month = Integer.parseInt(month_raw);

        SingleDAO d = new SingleDAO();
        List<Integer> listMonth = new ArrayList<>();
        HashMap<Integer, Float> map = new HashMap<>();
        if (month == 0) {
            for (int i = 1; i <= 12; i++) {
                map.put(i, d.resSales(i, year));
                listMonth.add(i);
            }
            map.put(13, d.resTotalSales());
        } else {
            map.put(month, d.resSales(month, year));
        }
        request.setAttribute("map", map);
        request.setAttribute("year", year);
        request.setAttribute("month", month);
        request.setAttribute("listMonth", listMonth);
        request.getRequestDispatcher("manage_4.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
