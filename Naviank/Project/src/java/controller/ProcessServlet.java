/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.MultiDAO;
import dal.SingleDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Cart;
import model.Dish;
import model.Item;

/**
 *
 * @author Naviank
 */
public class ProcessServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProcessServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProcessServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MultiDAO pdb = new MultiDAO();
        SingleDAO dao = new SingleDAO();
        List<Dish> list = pdb.getAllDish();
        Cookie arr[] = request.getCookies();
        String txt = "";
        for (Cookie o : arr) {
            if (o.getName().equals("cart")) {
                txt = txt + o.getValue();
                o.setMaxAge(0);
                response.addCookie(o);
            }
            Cart cart = new Cart(txt, list);
            String num_raw = request.getParameter("num");
            String id_raw = request.getParameter("id");
            int id, num = 0;
            try {
                id = Integer.parseInt(id_raw);
                Dish d = dao.getDishByID(id);
//                int numStore = d.getQuantity();
                num = Integer.parseInt(num_raw);
                if (num == -1 && (cart.getQuantityById(id) <= 1)) {
                    cart.removeItem(id);
                } else {
//                    if ((num == 1)/* && cart.getQuantityById(id) >= 100*/) {
//                        num = 0;
//                    }
                    double price = d.getPrice();
                    Item t = new Item(d, num, price);
                    cart.addItem(t);
                }
            } catch (NumberFormatException e) {
            }
            List<Item> items = cart.getItems();
            txt = "";
            if (!items.isEmpty()) {
                txt = items.get(0).getDish().getId() + ":" + items.get(0).getQuantity();
                for (int i = 1; i < items.size(); i++) {
                    txt = txt + "," + items.get(i).getDish().getId()
                            + ":" + items.get(i).getQuantity();
                }
            }
            Cookie c = new Cookie("cart", txt);
            c.setMaxAge(24 * 60 * 60);
            response.addCookie(c);
            request.setAttribute("cart", cart);
            request.getRequestDispatcher("mycart.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendRedirect("menu");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
