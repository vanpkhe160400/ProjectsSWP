/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SingleDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Category;
import model.Chef;
import model.Dish;

/**
 *
 * @author Naviank
 */
public class EditFoodServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditFoodServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditFoodServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SingleDAO dao = new SingleDAO();
        String type = request.getParameter("type");
        request.setAttribute("type", type);
        String id = request.getParameter("id");
        Category c;
        Dish d;
        if (type.equals("cate")) {
            c = dao.getCateByID(Integer.parseInt(id));
            request.setAttribute("data", c);
        } else {
            d = dao.getDishByID(Integer.parseInt(id));
            request.setAttribute("data", d);
        }
        request.getRequestDispatcher("inputeditfood.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SingleDAO dao = new SingleDAO();
        String type = request.getParameter("type");

        if (type.equals("cate")) {
            Category c = null;
            int chid = 0;
            String id = request.getParameter("id");
            String name = request.getParameter("name");
            String jname = request.getParameter("jname");
            String chefid = request.getParameter("chefid");
            try {
                c = dao.getCateByID(Integer.parseInt(id));
                chid = (Integer.parseInt(chefid));
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            if (c == null) {
                request.setAttribute("error", "Error");
                request.getRequestDispatcher("inputeditfood.jsp").forward(request, response);
            } else {
                if (name != null && name.length() > 0) {
                    c.setName(name);
                }
                if (jname != null && jname.length() > 0) {
                    c.setjName(jname);
                }
                if (chefid != null && chefid.length() > 0) {
                    c.setChef(dao.getChefByID(chid));
                }
                dao.updateCate(c);
//                request.getRequestDispatcher("menu").forward(request, response);
                response.sendRedirect("menu");
            }
        } else {
            Dish d = null;
            Category c = null;
            int id;
            boolean isnew = true;

            String old_raw = request.getParameter("cidold");
            String new_raw = request.getParameter("cid");
            String name = request.getParameter("name");
            String img = request.getParameter("img");
            String price_raw = request.getParameter("price");
            float price = 0;
            String des = request.getParameter("des");

            try {
                id = Integer.parseInt(request.getParameter("id"));
                if (new_raw == null || new_raw.length() == 0) {
                    c = dao.getCateByID(Integer.parseInt(old_raw));
                    isnew = false;
                } else {
                    c = dao.getCateByID(Integer.parseInt(new_raw));
                }
                price = Float.parseFloat(price_raw);
                d = dao.getDishByID(id);
            } catch (NumberFormatException e) {
                System.out.println(e);
            }

            if (c == null) {
                request.setAttribute("error", "This category is not in your restaurant");
                request.getRequestDispatcher("inputeditfood.jsp").forward(request, response);
            } else if (d == null) {
                request.setAttribute("error", "This dish is not in your restaurant");
                request.getRequestDispatcher("inputeditfood.jsp").forward(request, response);
            } else {
                if (isnew) {
                    d.setCat(c);
                }
                if (name != null && name.length() > 0) {
                    d.setName(name);
                }
                if (img != null && img.length() > 0) {
                    d.setImg(img);
                }
                if (price_raw != null && price_raw.length() > 0) {
                    d.setPrice(price);
                }
                if (des != null && des.length() > 0) {
                    d.setDescribe(des);
                }
                dao.updateDish(d);
//                    request.getRequestDispatcher("menu").forward(request, response);
                response.sendRedirect("menu");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
