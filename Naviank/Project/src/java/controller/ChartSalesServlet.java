/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.MultiDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.awt.Color;
import java.io.OutputStream;
import java.util.List;
import javax.swing.BorderFactory;
import model.Dish;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Naviank
 */
public class ChartSalesServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChartSalesServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChartSalesServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DefaultPieDataset dataset = createDataset();
        JFreeChart chart = createChart(dataset);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
        chartPanel.setBackground(Color.white);

        response.setContentType("saleschart.png");
        OutputStream outputStream = response.getOutputStream();
        ChartUtilities.writeChartAsPNG(outputStream, chart, 1200, 800);
//        request.getRequestDispatcher("chart.jsp").forward(request, response);
    }

    private DefaultPieDataset createDataset() {
        DefaultPieDataset dataset = new DefaultPieDataset();
        MultiDAO dao = new MultiDAO();
        List<Dish> list = dao.maxSalesDish();
        for (Dish d : list) {
            dataset.setValue(d.getName(), d.getSales());
        }
        return dataset;
    }

    private JFreeChart createChart(DefaultPieDataset dataset) {
        JFreeChart pieChart = ChartFactory.createPieChart("Value of dishes", dataset, true, true, true);
//        JFreeChart barChart = ChartFactory.createBarChart(
//                "Number of orders by day",
//                "",
//                "Orders",
//                dataset,
//                PlotOrientation.VERTICAL,
//                false, true, false);
//
        return pieChart;
    }
}
