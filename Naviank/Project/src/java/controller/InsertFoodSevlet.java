/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.MultiDAO;
import dal.SingleDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Category;
import model.Chef;
import model.Dish;

/**
 *
 * @author Naviank
 */
public class InsertFoodSevlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InsertCateSevler</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InsertCateSevler at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MultiDAO dao = new MultiDAO();
        List<Category> list = dao.getAllCate();
        request.setAttribute("listCate", list);
        String type = request.getParameter("type");
        request.setAttribute("type", type);
        request.getRequestDispatcher("inputinsertfood.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SingleDAO dao = new SingleDAO();
        String type = request.getParameter("type");

        if (type.equals("cate")) {
            String name = request.getParameter("name");
            String jname = request.getParameter("jname");
            String chefid = request.getParameter("chefid");
            Chef ch = null;
            try {
                ch = dao.getChefByID(Integer.parseInt(chefid));
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            if (ch == null) {
                request.setAttribute("error", "This chef is not in your restaurant yet!");
                request.getRequestDispatcher("inputinsertfood.jsp").forward(request, response);
            } else {
                Category c = new Category(0, name, jname, ch);
                dao.insertCate(c);
//                request.getRequestDispatcher("menu").forward(request, response);
                response.sendRedirect("menu");
            }
        } else {
            String cid = request.getParameter("cid");
            String name = request.getParameter("name");
            String img = request.getParameter("img");
            String price = request.getParameter("price");
            String des = request.getParameter("des");
            Category c = null;
            try {
                c = dao.getCateByID(Integer.parseInt(cid));
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
            if (c == null) {
                request.setAttribute("error", "This category is not in your restaurant");
                request.getRequestDispatcher("inputinsertfood.jsp").forward(request, response);
            } else {
                Dish d = new Dish(0, c, name, img, Float.parseFloat(price), des);
                dao.insertDish(d);
//                request.getRequestDispatcher("menu_1.jsp").forward(request, response);
                response.sendRedirect("menu");
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
