<%-- 
    Document   : detailchef.jsp
    Created on : Mar 14, 2023, 8:21:21 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Staff detail Page</title>
    </head>
    <body>
        <c:set var="st" value="${requestScope.staffselected}"/>
        <c:set var="ch" value="Chef"/>
        <c:set var="sh" value="Shipper"/>
        <c:set var="cln" value="${st.getClass().getSimpleName()}"/>

        <c:if test="${cln.equals(ch)}">
            <h1 style="color: red;">Chef: ${st.name}</h1>
            <h4>Certificate: ${st.certi}</h4> 
            <img src="${st.img}" alt="anh chef"/>
        </c:if>
        <c:if test="${cln.equals(sh)}">
            <h1 style="color: red;">Shipper: ${st.name}</h1>
            <h4>Phone number: ${st.phone}</h4> 
            <img src="${st.img}" alt="anh chef"/>
        </c:if>


    </body>
</html>
