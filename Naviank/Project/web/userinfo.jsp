<%-- 
    Document   : home.jsp
    Created on : Mar 9, 2023, 5:05:43 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Html.html to edit this template
-->

<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <head>

        <!-- Basic Page Needs
      ================================================== -->
        <meta charset="utf-8">
        <title>JapaneseFood | Template by Naviank</title>
        <meta name="description" content="Free Responsive Html5 Css3 Templates | zerotheme.com">
        <meta name="author" content="www.zerotheme.com">

        <!-- Mobile Specific Metas
      ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS
      ================================================== -->
        <link rel="stylesheet" href="css/zerogrid.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/slide.css">
        <link rel="stylesheet" href="css/menu.css">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
        <!--[if lt IE 9]>
                    <script src="js/html5.js"></script>
                    <script src="js/css3-mediaqueries.js"></script>
            <![endif]-->

        <style>
            .box-2 {
                margin-top: 50px;
                background-image: url("images/homebg.png");
                background-repeat: round;
            }
            .box-2 center h1{
                display: block;
                color: red;
                font-size: 48px;
                margin-bottom: 40px;
            }

            .box-2 #cusinfo {
                font-size: medium;
                margin: 8px 0;
            }
        </style>

    </head>
    <body>
        <div class="wrap-body">
            <!--///////////////////////////////////////Top-->
            <div class="top">
                <div class="zerogrid">
                    <ul class="number f-left">
                        <li class="mail"><p>vanpkhe160400@fpt.edu.vn</p></li>
                        <li class="phone"><p>038.447.8185</p></li>
                    </ul>
                    <ul class="top-social f-right">
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/naviank.pham.2407/"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <c:if test="${sessionScope.admin==null}">
                            <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="login"><i class="fa">I</i></a></li>
                            <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="signup"><i class="fa">S</i></a></li>
                            </c:if>
                            <c:if test="${sessionScope.admin!=null}">
                            <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="logout"><i class="fa">O</i></a></li>
                            <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="userinfo"><i class="fa">U</i></a></li>
                            <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="show"><i class="fa">C</i></a></li>
                            </c:if>
                    </ul>
                </div>
            </div>
            <!--////////////////////////////////////Header-->
            <header>
                <div class="zerogrid">
                    <center><div class="logo"><img src="images/logo.png"></div></center>
                </div>
            </header>
            <div class="site-title">
                <div class="zerogrid">
                    <div class="row">
                        <h2 class="t-center">Best Japanese Restaurant in Ha Noi</h2>
                    </div>
                </div>
            </div>
            <!--//////////////////////////////////////Menu-->
            <a href="#" class="nav-toggle">Toggle Navigation</a>
            <nav class="cmn-tile-nav">
                <ul class="clearfix">
                    <li class="colour-1"><a href="home">Home</a></li>
                    <li class="colour-2"><a href="menu">Menu</a></li>
                    <li class="colour-3"><a href="location">Location</a></li>
                    <li class="colour-4"><a href="archive">Blog</a></li>
                    <li class="colour-5"><a href="reservation">Reservation</a></li>
                    <li class="colour-6"><a href="staff">Our Staff</a></li>

                    <c:if test="${sessionScope.admin==null}">

                        <li class="colour-7"><a href="#">Manager</a></li>

                    </c:if>
                    <c:if test="${sessionScope.admin.isAdmin}">

                        <li class="colour-7"><a href="morder">Manager</a></li>

                    </c:if>
                    <li class="colour-8"><a href="gallery">Gallery</a></li>
                </ul>
            </nav>
            <!--
                        <div class="zerogrid">
                            <div class="callbacks_container">
                                <ul class="rslides" id="slider4">
                                    <li>
                                        <img src="images/banner1.jpg" alt="">
                                        <div class="caption">
                                            <h2>We've got the best spareribs in town.</h2></br>
                                            <p>#Description#</p>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="images/banner2.jpg" alt="">
                                        <div class="caption">
                                            <h2>If food is an experience, then you'll find it at the restaurant.</h2></br>
                                            <p>#Description#</p>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="images/banner3.jpg" alt="">
                                        <div class="caption">
                                            <h2>Enjoy our take-away menu.</h2></br>
                                            <p>#Description#</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>-->

            <!--////////////////////////////////////Container-->
            <section id="container" class="index-page">
                <div class="wrap-container zerogrid">
                    <!-----------------content-box-1-------------------->
                    <!--                    <section class="content-box box-1">
                                            <div class="zerogrid">
                                                <div class="row box-item">Start Box
                                                    <h2>“Best food, best serve”</h2>
                                                    <span>Unc elementum lacus in gravida pellentesque urna dolor eleifend felis eleifend</span>
                                                </div>
                                            </div>
                                        </section>-->
                    <!-----------------content-box-2-------------------->
                    <section class="content-box box-2">
                        <div class="zerogrid">
                            <div class="row wrap-box"><!--Start Box-->
                                <div class="header">
                                    <h2>Welcome</h2>
                                    <hr class="line">
                                    <span>Welcome to our restaurant!</span>
                                </div>
                            </div>
                            <center>
                                <c:set var="c" value="${requestScope.curcus}"/>
                                <h1>User information</h1>
                                <c:if test="${curcus.isAdmin}">
                                    <div id="cusinfo">Admin</div>
                                </c:if>
                                <c:if test="${!curcus.isAdmin}">
                                    <div id="cusinfo">Customer</div>
                                </c:if>                                
                                <div id="cusinfo">User:   ${c.firstName} ${c.lastName}</div>
                                <div id="cusinfo">Login name:   ${c.username}</div>
                                <div id="cusinfo">User address:   ${c.address}</div>
                                <div id="cusinfo">User phone:   ${c.phone}</div>
                                <div id="cusinfo">User email:   ${c.email}</div>

                                <button style="margin-top: 8px;"><a href="home" style="text-decoration: none;">Home</a></button>
                                <button style="margin-top: 8px;"><a href="changeinfo" style="text-decoration: none;">Edit</a></button>
                            </center>
                        </div>
                    </section>
                </div>
            </section>

            <!--////////////////////////////////////Footer-->
            <footer class="zerogrid">
                <div class="wrap-footer">
                    <div class="row">
                        <div class="col-1-3">
                            <div class="wrap-col">
                                <h4>Customer Testimonials</h4>
                                <div class="row">
                                    <img src="images/pkv.jpg" style='width: 80px;'>
                                    <h5>Naviank.Pham</h5>
                                    <p>Siêu cấp đẹp trai, dễ thương. K16-SE FPTU. Yêu NKL nhất trần đời. Fan BlackPink, fan T-ara, Itzy, IZONE. 1Champ Katarina. Thích xem siêu nhân.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-1-3">
                            <div class="wrap-col">
                                <h4>Location</h4>
                                <div class="wrap-map">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d54124.04099762778!2d105.53419497509476!3d21.002061948575562!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345b465a4e65fb%3A0xaae6040cfabe8fe!2sFPT%20University!5e0!3m2!1sen!2sus!4v1678368781445!5m2!1sen!2sus" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="col-1-3">
                            <div class="wrap-col">
                                <h4>Open Daily</h4>
                                <p><span>Mon.</span> 17:00 - 21:00</p>
                                <p><span>Tue.-Wed.</span> 16:30 – 21:00</p>
                                <p><span>Thu.-Sat.</span> 16:30 – 21:00</p>
                                <p><span>Sun.</span> 11:00 – 21:00</p>
                                <p><span>Need help getting home?</span></br>
                                    We will call a cab for you!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="copyright">
                    <div class="wrapper">
                        Copyright 2023- Designed by <a href="https://www.facebook.com/naviank.pham.2407" title="free website templates">Naviank</a>
                        <ul class="quick-link f-right">
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms of Use</a></li>
                        </ul>
                    </div>
                </div>
            </footer>


            <!-- js -->
            <script src="js/classie.js"></script>
            <script src="js/demo.js"></script>

            <script src="js/jquery-1.11.3.min.js"></script>
            <script src="js/responsiveslides.min.js"></script>
            <script>
                $(function () {
                    // Slideshow 4
                    $("#slider4").responsiveSlides({
                        auto: true,
                        pager: false,
                        nav: false,
                        speed: 500,
                        namespace: "callbacks",
                        before: function () {
                            $('.events').append("<li>before event fired.</li>");
                        },
                        after: function () {
                            $('.events').append("<li>after event fired.</li>");
                        }
                    });
                });
            </script>
        </div>
    </body></html>