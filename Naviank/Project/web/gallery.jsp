<%-- 
    Document   : home.jsp
    Created on : Mar 9, 2023, 5:05:43 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Html.html to edit this template
-->

<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <head>

        <!-- Basic Page Needs
      ================================================== -->
        <meta charset="utf-8">
        <title>JapaneseFood | Template by Naviank</title>
        <meta name="description" content="Free Responsive Html5 Css3 Templates | zerotheme.com">
        <meta name="author" content="www.zerotheme.com">

        <!-- Mobile Specific Metas
      ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS
      ================================================== -->
        <link rel="stylesheet" href="css/zerogrid.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/slide.css">
        <link rel="stylesheet" href="css/menu.css">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
        <!--[if lt IE 9]>
                    <script src="js/html5.js"></script>
                    <script src="js/css3-mediaqueries.js"></script>
            <![endif]-->

        <style>
            #main-content {
                background-image: url(images/homebg.png);
                background-repeat: round;
            }

            .zoom-container {
                position: relative;
                overflow: hidden;
                display: inline-block;
                font-size: 16px;
                font-size: 1rem;
                vertical-align: top;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
            }
            .zoom-container img {
                display: block;
                width: 200px;
                height: auto;
                -webkit-transition: all .5s ease; /* Safari and Chrome */
                -moz-transition: all .5s ease; /* Firefox */
                -ms-transition: all .5s ease; /* IE 9 */
                -o-transition: all .5s ease; /* Opera */
                transition: all .5s ease;

            }
            .zoom-container .zoom-caption {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                z-index: 10;
                background: rgba(0, 0, 0, .5);
                -webkit-transition: all .5s ease; /* Safari and Chrome */
                -moz-transition: all .5s ease; /* Firefox */
                -ms-transition: all .5s ease; /* IE 9 */
                -o-transition: all .5s ease; /* Opera */
                transition: all .5s ease;
            }
            .zoom-container .zoom-caption span {
                display: block;
                text-align: center;
                font-family: 'Source Sans Pro', sans-serif;
                font-size: 1.5em;
                font-weight: 900;
                letter-spacing: -1px;
                text-transform: uppercase;
                color: #fff;
                margin: 23% 0 0;
                padding: 10px 0;
                border-top: 5px solid rgba(255, 255, 255, .15);
                border-bottom: 5px solid rgba(255, 255, 255, .15);
            }
            .zoom-container:hover img {
                -webkit-transform:scale(1.25); /* Safari and Chrome */
                -moz-transform:scale(1.25); /* Firefox */
                -ms-transform:scale(1.25); /* IE 9 */
                -o-transform:scale(1.25); /* Opera */
                transform:scale(1.25);
            }
            .zoom-container:hover .zoom-caption {
                background: none;
            }

            .crumbs ul {
                list-style: none;
                display: flex;
                margin-bottom: 8px;
                justify-content: space-between;
            }
            .private {
                font-size: 24px;
                <c:if test="${!sessionScope.admin.isAdmin}">
                    display: none;
                </c:if>
                margin: 0;
                padding:  0;
            }
            .private p {
                padding: 4px;
                border: 2px solid black;
            </style>

        </head>
        <body>
            <div class="wrap-body">
                <!--///////////////////////////////////////Top-->
                <div class="top">
                    <div class="zerogrid">
                        <ul class="number f-left">
                            <li class="mail"><p>vanpkhe160400@fpt.edu.vn</p></li>
                            <li class="phone"><p>038.447.8185</p></li>
                        </ul>
                        <ul class="top-social f-right">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.facebook.com/naviank.pham.2407/"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <c:if test="${sessionScope.admin==null}">
                                <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="login"><i class="fa">I</i></a></li>
                                <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="signup"><i class="fa">S</i></a></li>
                                </c:if>
                                <c:if test="${sessionScope.admin!=null}">
                                <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="logout"><i class="fa">O</i></a></li>
                                <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="userinfo"><i class="fa">U</i></a></li>
                                <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="show"><i class="fa">C</i></a></li>
                                </c:if>
                        </ul>
                    </div>
                </div>
                <!--////////////////////////////////////Header-->
                <header>
                    <div class="zerogrid">
                        <center><div class="logo"><img src="images/logo.png"></div></center>
                    </div>
                </header>
                <div class="site-title">
                    <div class="zerogrid">
                        <div class="row">
                            <h2 class="t-center">Best Japanese Restaurant in Ha Noi</h2>
                        </div>
                    </div>
                </div>
                <!--//////////////////////////////////////Menu-->
                <a href="#" class="nav-toggle">Toggle Navigation</a>
                <nav class="cmn-tile-nav">
                    <ul class="clearfix">
                        <li class="colour-1"><a href="home">Home</a></li>
                        <li class="colour-2"><a href="menu">Menu</a></li>
                        <li class="colour-3"><a href="location">Location</a></li>
                        <li class="colour-4"><a href="archive">Blog</a></li>
                        <li class="colour-5"><a href="reservation">Reservation</a></li>
                        <li class="colour-6"><a href="staff">Our Staff</a></li>

                        <c:if test="${sessionScope.admin==null || !sessionScope.admin.isAdmin}">

                            <li class="colour-7"><a href="#">News</a></li>

                        </c:if>
                        <c:if test="${sessionScope.admin.isAdmin}">

                            <li class="colour-7"><a href="morder">Manager</a></li>

                        </c:if>
                        <li class="colour-8"><a href="gallery">Gallery</a></li>
                    </ul>
                </nav>

                <!--////////////////////////////////////Container-->
                <section id="container" class="sub-page">
                    <div class="wrap-container zerogrid">
                        <div class="crumbs">
                            <ul>
                                <li><a href="home">Home</a></li>
                                <li><a href="dishchart">Dishes Quantity Chart</a></li>
                                <li><a href="orderchart">Orders Chart</a></li>
                                <li><a href="saleschart">Dishes Sales Chart</a></li>
                            </ul>
                        </div>
                        <div id="main-content">
                            <div class="wrap-content">
                                <div class="private">
                                    <p>Restaurant sales: ${requestScope.sales}</p><br/>
                                    <p>Most expensive orders: ${requestScope.mostvalue}</p><br/>
                                    <p>Spend most user: ${requestScope.spendMost}</p><br/>
                                    <p>Dish has max sales: ${requestScope.maxSales.name}</p><br/>
                                </div>

                                <div class="row">
                                    <c:forEach items="${requestScope.listDish}" var="d">
                                        <div class="col-1-4">
                                            <div class="zoom-container">
                                                <a href="detail?dishid=${d.id}">
                                                    <span class="zoom-caption">
                                                        <span>${d.id}</span>
                                                    </span>
                                                    <img style="width: 3000px;
                                                    height: 200px;
                                                    border: 2px solid white;" src="${d.img}"/>
                                                </a>
                                            </div>
                                        </div>   
                                    </c:forEach>
                                </div>
                            </div>
                        </div> 
                    </div>
                </section>


                <!--////////////////////////////////////Footer-->
                <footer class="zerogrid">
                    <div class="wrap-footer">
                        <div class="row">
                            <div class="col-1-3">
                                <div class="wrap-col">
                                    <h4>Customer Testimonials</h4>
                                    <div class="row">
                                        <img src="images/pkv.jpg" style='width: 80px;'>
                                        <h5>Naviank.Pham</h5>
                                        <p>Siêu cấp đẹp trai, dễ thương. K16-SE FPTU. Yêu NKL nhất trần đời. Fan BlackPink, fan T-ara, Itzy, IZONE. 1Champ Katarina. Thích xem siêu nhân.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1-3">
                                <div class="wrap-col">
                                    <h4>Location</h4>
                                    <div class="wrap-map">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d54124.04099762778!2d105.53419497509476!3d21.002061948575562!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345b465a4e65fb%3A0xaae6040cfabe8fe!2sFPT%20University!5e0!3m2!1sen!2sus!4v1678368781445!5m2!1sen!2sus" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1-3">
                                <div class="wrap-col">
                                    <h4>Open Daily</h4>
                                    <p><span>Mon.</span> 17:00 - 21:00</p>
                                    <p><span>Tue.-Wed.</span> 16:30 – 21:00</p>
                                    <p><span>Thu.-Sat.</span> 16:30 – 21:00</p>
                                    <p><span>Sun.</span> 11:00 – 21:00</p>
                                    <p><span>Need help getting home?</span></br>
                                        We will call a cab for you!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="copyright">
                        <div class="wrapper">
                            Copyright 2023- Designed by <a href="https://www.facebook.com/naviank.pham.2407" title="free website templates">Naviank</a>
                            <ul class="quick-link f-right">
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms of Use</a></li>
                            </ul>
                        </div>
                    </div>
                </footer>


                <!-- js -->
                <script src="js/classie.js"></script>
                <script src="js/demo.js"></script>

                <script src="js/jquery-1.11.3.min.js"></script>
                <script src="js/responsiveslides.min.js"></script>
                <script>
                    $(function () {
                        // Slideshow 4
                        $("#slider4").responsiveSlides({
                            auto: true,
                            pager: false,
                            nav: false,
                            speed: 500,
                            namespace: "callbacks",
                            before: function () {
                                $('.events').append("<li>before event fired.</li>");
                            },
                            after: function () {
                                $('.events').append("<li>after event fired.</li>");
                            }
                        });
                    });
                </script>
            </div>
        </body></html>