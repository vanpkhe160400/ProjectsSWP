<%-- 
    Document   : menu.jsp
    Created on : Mar 9, 2023, 8:17:16 PM
    Author     : Naviank
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <head>

        <!-- Basic Page Needs
      ================================================== -->
        <meta charset="UTF-8">
        <title>Japanese Food | Template by Naviank</title>
        <meta name="description" content="Free Responsive Html5 Css3 Templates | zerotheme.com">
        <meta name="author" content="www.zerotheme.com">

        <!-- Mobile Specific Metas
      ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS
      ================================================== -->
        <link rel="stylesheet" href="css/zerogrid.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/slide.css">
        <link rel="stylesheet" href="css/menu.css">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
        <!--[if lt IE 9]>
                    <script src="js/html5.js"></script>
                    <script src="js/css3-mediaqueries.js"></script>
            <![endif]-->
        <style>

            footer p{
                color: #442000;
                text-shadow: 1px 1px 1px #FFFFFF;
                font-size: 12px;
                font-family: sans-serif;
                font-style: italic;
                text-transform: uppercase;
                margin: 15px 0;
            }
            .crumbs {
                -webkit-box-shadow: 0px 25px 18px -26px rgba(0,0,0,0.75);
                -moz-box-shadow: 0px 25px 18px -26px rgba(0,0,0,0.75);
                box-shadow: 0px 25px 18px -26px rgba(0,0,0,0.75);
                padding:8px;
            }
            .crumbs ul {
                list-style: none;
                display: inline-table;
            }
            .crumbs ul li {
                display: inline;
            }
            .crumbs ul li a {
                display: block;
                float: left;
                background: #654e2d;
                text-align: center;
                padding: 7px 30px 8px 50px;
                position: relative;
                margin: 0 20px 0 0;
                font-size: 20px;
                text-decoration: none;
                color: #ddd;
            }
            .crumbs ul li a:after {
                content: "";
                border-top: 20px solid transparent;
                border-bottom: 20px solid transparent;
                border-left: 20px solid #654e2d;
                position: absolute;
                right: -20px;
                top: 0;
                z-index: 1;
                -webkit-transition: border 0.3s, -webkit-transform 0.3s;
                -moz-transition: border 0.3s, -moz-transform 0.3s;
                -o-transition: border 0.3s, -o-transform 0.3s;
                transition: border 0.3s, transform 0.3s;
            }
            .crumbs ul li a:before {
                content: "";
                border-top: 20px solid transparent;
                border-bottom: 20px solid transparent;
                border-left: 20px solid #fff;
                position: absolute;
                left: 0;
                top: 0;
            }
            .crumbs ul li:first-child a {
                border-top-left-radius: 6px;
                border-bottom-left-radius: 6px;
            }
            .crumbs ul li:first-child a:before {
                display: none;
            }
            .crumbs ul li:last-child a {
                padding-right: 50px;
                border-top-right-radius: 6px;
                border-bottom-right-radius: 6px;
            }
            .crumbs ul li:last-child a:after {
                display: none;
            }
            .crumbs ul li a:hover {
                background: #222;
                color: #fff;
            }
            .crumbs ul li a:hover:after {
                border-left-color: #222;
            }
            input {
                border: 1px solid;
                min-width: 100px;
            }
            .center {
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 80%;
            }
        </style>
    </head>
    <body>
        <div class="wrap-body">
            <!--///////////////////////////////////////Top-->
            <div class="top">
                <div class="zerogrid">
                    <ul class="number f-left">
                        <li class="mail"><p>Cbnlolivan@gmail.com</p></li>
                        <li class="phone"><p>038 447 8185</p></li>
                    </ul>
                    <ul class="top-social f-right">
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/naviank.pham.2407"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>

                        <c:if test="${sessionScope.admin==null}">

                            <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="login"><i class="fa">I</i></a></li>
                            <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="signup"><i class="fa">S</i></a></li>

                        </c:if>
                        <c:if test="${sessionScope.admin!=null}">
                            <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="logout"><i class="fa">O</i></a></li>
                            <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="userinfo"><i class="fa">U</i></a></li>
                            <li><a class="getstarted scrollto" style="background-color: blueviolet;" href="show"><i class="fa">C</i></a></li>

                        </c:if>                        
                    </ul>
                </div>
            </div>
            <!--////////////////////////////////////Header-->
            <header>
                <div class="zerogrid">
                    <center><div class="logo"><img src="images/logo.png"></div></center>
                </div>
            </header>
            <div class="site-title">
                <div class="zerogrid">
                    <div class="row">
                        <h2 class="t-center">Best Japanese Restaurant in Ha Noi</h2>
                    </div>
                </div>
            </div>
            <!--//////////////////////////////////////Menu-->
            <a href="#" class="nav-toggle">Toggle Navigation</a>
            <nav class="cmn-tile-nav">
                <ul class="clearfix">
                    <li class="colour-1"><a href="home">Home</a></li>
                    <li class="colour-2"><a href="menu">Menu</a></li>
                    <li class="colour-3"><a href="location">Location</a></li>
                    <li class="colour-4"><a href="archive.html">Blog</a></li>
                    <li class="colour-5"><a href="reservation.html">Reservation</a></li>
                    <li class="colour-6"><a href="staff">Our Staff</a></li>
                    <li class="colour-7"><a href="news.html">News</a></li>
                    <li class="colour-8"><a href="gallery.html">Gallery</a></li>
                </ul>
            </nav>

            <!--////////////////////////////////////Container-->
            <section id="container" class="sub-page">
                <div class="wrap-container zerogrid">
                    <div class="crumbs">
                        <ul>
                            <li><a href="menu">Menu</a></li>
                            <li><a href="searchname">Search Name</a></li>
                            <li><a href="searchcat">Search Category</a></li>
                        </ul>
                    </div>

                    <div id="main-content">
                        <div id="wrap-content">
                            <c:set var="d" value="${requestScope.dishselected}"/>
                            <div class="wrap-col">
                                <h3>${d.name}</h3>
                                <p> Dish type: ${d.cat.name}</p>
                                <span>${d.price} $</span>
                                <form action="addcart">
                                    <label for="quantity">Quantity (between 1 and 30):</label>
                                    <input type="hidden" name="id" value="${d.id}"/>
                                    <input type="number" id="quantity" name="quantity" min="1" max="30" value="1">
                                    <input type="submit" value="Add to cart"/>
                                </form>
                                <br/>
                                <div class="post" style="display: grid;">
                                    <a href="${d.img}"><img class="center" style="margin-left: auto; margin-right: auto; display: block; padding:0; width: 1200px; height: auto;" src="${d.img}"/></a>
                                    <div class="wrapper">
                                        <h7>${d.describe}</a></h7>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--////////////////////////////////////Footer-->
            <footer class="zerogrid">
                <div class="wrap-footer">
                    <div class="row">
                        <div class="col-1-3">
                            <div class="wrap-col">
                                <h4>Customer Testimonials</h4>
                                <div class="row">
                                    <img src="images/pkv.jpg" style='width: 80px;'>
                                    <h5>Naviank.Pham</h5>
                                    <p style="font-size: 8px;">Siêu cấp đẹp trai, dễ thương. K16-SE FPTU. Yêu NKL nhất trần đời. Fan BlackPink, fan T-ara, Itzy, IZONE. 1Champ Katarina. Thích xem siêu nhân.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-1-3">
                            <div class="wrap-col">
                                <h4>Location</h4>
                                <div class="wrap-map">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d54124.04099762778!2d105.53419497509476!3d21.002061948575562!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31345b465a4e65fb%3A0xaae6040cfabe8fe!2sFPT%20University!5e0!3m2!1sen!2sus!4v1678368781445!5m2!1sen!2sus" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="col-1-3">
                            <div class="wrap-col">
                                <h4>Open Daily</h4>
                                <p><span>Mon.</span> 17:00 - 21:00</p>
                                <p><span>Tue.-Wed.</span> 16:30 – 21:00</p>
                                <p><span>Thu.-Sat.</span> 16:30 – 21:00</p>
                                <p><span>Sun.</span> 11:00 – 21:00</p>
                                <p><span>Need help getting home?</span></br>
                                    We will call a cab for you!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="copyright">
                    <div class="wrapper">
                        Copyright 2023- Designed by <a href="https://www.facebook.com/naviank.pham.2407" title="free website templates">Naviank</a>
                        <ul class="quick-link f-right">
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms of Use</a></li>
                        </ul>
                    </div>
                </div>
            </footer>


            <!-- js -->
            <script src="js/classie.js"></script>
            <script src="js/demo.js"></script>
        </div>
    </body>
</html>