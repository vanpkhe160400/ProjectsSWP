<%-- 
    Document   : search.jsp
    Created on : Mar 13, 2023, 7:38:08 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="" method="post">
            <input type="text" name="searchvalue"/> 
            <input type="submit" value="Search"/>
        </form>
    </body>
</html>
