USE [master]
GO
/****** Object:  Database [JapaneseRestaurant]    Script Date: 3/23/2023 8:54:37 AM ******/
CREATE DATABASE [JapaneseRestaurant]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'JapaneseRestaurant', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\JapaneseRestaurant.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'JapaneseRestaurant_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\JapaneseRestaurant_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [JapaneseRestaurant] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [JapaneseRestaurant].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [JapaneseRestaurant] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET ARITHABORT OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [JapaneseRestaurant] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [JapaneseRestaurant] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET  ENABLE_BROKER 
GO
ALTER DATABASE [JapaneseRestaurant] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [JapaneseRestaurant] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET RECOVERY FULL 
GO
ALTER DATABASE [JapaneseRestaurant] SET  MULTI_USER 
GO
ALTER DATABASE [JapaneseRestaurant] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [JapaneseRestaurant] SET DB_CHAINING OFF 
GO
ALTER DATABASE [JapaneseRestaurant] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [JapaneseRestaurant] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [JapaneseRestaurant] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [JapaneseRestaurant] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'JapaneseRestaurant', N'ON'
GO
ALTER DATABASE [JapaneseRestaurant] SET QUERY_STORE = OFF
GO
USE [JapaneseRestaurant]
GO
/****** Object:  Table [dbo].[BTables]    Script Date: 3/23/2023 8:54:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BTables](
	[Table_ID] [int] IDENTITY(1,1) NOT NULL,
	[TimeFrom] [time](7) NULL,
	[TimeTO] [time](7) NULL,
	[Seats] [int] NOT NULL,
	[OnBook] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Table_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 3/23/2023 8:54:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Cat_ID] [int] IDENTITY(1,1) NOT NULL,
	[Cat_Name] [nvarchar](255) NOT NULL,
	[Cat_JName] [nvarchar](255) NULL,
	[Chef_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Cat_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Chefs]    Script Date: 3/23/2023 8:54:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chefs](
	[Chef_ID] [int] IDENTITY(1,1) NOT NULL,
	[Chef_Name] [nvarchar](255) NOT NULL,
	[Chef_Certi] [nvarchar](255) NOT NULL,
	[Chef_Img] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Chef_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dishes]    Script Date: 3/23/2023 8:54:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dishes](
	[Dish_ID] [int] IDENTITY(1,1) NOT NULL,
	[Cat_ID] [int] NOT NULL,
	[Dish_Name] [nvarchar](255) NOT NULL,
	[Dish_Image] [nvarchar](255) NULL,
	[Dish_Price] [float] NOT NULL,
	[Dish_Describe] [ntext] NULL,
PRIMARY KEY CLUSTERED 
(
	[Dish_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetails]    Script Date: 3/23/2023 8:54:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetails](
	[number] [int] IDENTITY(1,1) NOT NULL,
	[Order_ID] [int] NOT NULL,
	[Dish_ID] [int] NOT NULL,
	[Quantity] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 3/23/2023 8:54:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Order_ID] [int] IDENTITY(1,1) NOT NULL,
	[U_ID] [int] NOT NULL,
	[OrderDate] [date] NOT NULL,
	[OrderTime] [time](7) NULL,
	[Shipped] [bit] NULL,
	[ShipAddress] [nvarchar](255) NULL,
	[Shipper_ID] [int] NOT NULL,
	[Total_money] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[Order_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Restaurant]    Script Date: 3/23/2023 8:54:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Restaurant](
	[sales] [float] NULL,
	[current_month] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shippers]    Script Date: 3/23/2023 8:54:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shippers](
	[Shipper_ID] [int] IDENTITY(1,1) NOT NULL,
	[Shipper_Name] [nvarchar](255) NOT NULL,
	[Shipper_Phone] [nvarchar](255) NOT NULL,
	[Shipper_Img] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Shipper_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 3/23/2023 8:54:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[U_ID] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](255) NOT NULL,
	[password] [nvarchar](255) NOT NULL,
	[U_FirstName] [nvarchar](255) NOT NULL,
	[U_LastName] [nvarchar](255) NULL,
	[U_Address] [nvarchar](255) NOT NULL,
	[U_Phone] [nvarchar](255) NOT NULL,
	[U_Email] [nvarchar](255) NULL,
	[IsAdmin] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[U_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[BTables] ON 

INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (1, NULL, NULL, 4, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (2, NULL, NULL, 4, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (3, NULL, NULL, 4, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (4, NULL, NULL, 4, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (5, NULL, NULL, 4, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (6, NULL, NULL, 4, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (7, NULL, NULL, 6, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (8, NULL, NULL, 10, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (9, NULL, NULL, 6, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (10, NULL, NULL, 4, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (11, NULL, NULL, 4, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (12, NULL, NULL, 4, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (13, NULL, NULL, 4, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (14, NULL, NULL, 4, 0)
INSERT [dbo].[BTables] ([Table_ID], [TimeFrom], [TimeTO], [Seats], [OnBook]) VALUES (15, NULL, NULL, 4, 0)
SET IDENTITY_INSERT [dbo].[BTables] OFF
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (1, N'Grilled and pan-fried dishes', N'Yakimono', 6)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (2, N'Stewed/simmered/cooked/boiled', N'Nimono', 3)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (3, N'Stir-fried dishes', N'Itamemono', 6)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (4, N'Steamed dishes', N'Mushimono', 4)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (5, N'Deep-fried dishes', N'Agemono', 6)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (6, N'Sliced raw fish', N'Sashimi', 5)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (7, N'Soups', N'Suimono', 3)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (8, N'Sweets', N'Wagashi', 1)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (9, N'Delicacies', N'Chinmi ', 1)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (10, N'Dishes dressed with various kinds of sauce', N'Aemono ', 2)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (11, N'Hot pot', N'Nabemono', 3)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (12, N'Sushi', N'Sushi', 2)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (13, N'Noodles', N'Men', 5)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (14, N'Rice', N'Meshi', 4)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (15, N'Meal', N'Setto', 1)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (16, N'Drink', N'Nomimono', 1)
INSERT [dbo].[Categories] ([Cat_ID], [Cat_Name], [Cat_JName], [Chef_ID]) VALUES (17, N'Na', N'Na', 3)
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[Chefs] ON 

INSERT [dbo].[Chefs] ([Chef_ID], [Chef_Name], [Chef_Certi], [Chef_Img]) VALUES (1, N'Pham Khai Van', N'FPTU', N'JapaneseFoodStaff-img\chefimg1.png')
INSERT [dbo].[Chefs] ([Chef_ID], [Chef_Name], [Chef_Certi], [Chef_Img]) VALUES (2, N'Nguyen Khanh Linh', N'FTU', N'JapaneseFoodStaff-img\chefimg2.png')
INSERT [dbo].[Chefs] ([Chef_ID], [Chef_Name], [Chef_Certi], [Chef_Img]) VALUES (3, N'Nguyen Van Kien', N'FPTU', N'JapaneseFoodStaff-img\chefimg3.png')
INSERT [dbo].[Chefs] ([Chef_ID], [Chef_Name], [Chef_Certi], [Chef_Img]) VALUES (4, N'Doan Duc Loc', N'FTU', N'JapaneseFoodStaff-img\chefimg4.png')
INSERT [dbo].[Chefs] ([Chef_ID], [Chef_Name], [Chef_Certi], [Chef_Img]) VALUES (5, N'Vu Khanh Linh', N'FPTU', N'JapaneseFoodStaff-img\chefimg5.png')
INSERT [dbo].[Chefs] ([Chef_ID], [Chef_Name], [Chef_Certi], [Chef_Img]) VALUES (6, N'Pham Minh Tam', N'NEU', N'JapaneseFoodStaff-img\chefimg6.png')
SET IDENTITY_INSERT [dbo].[Chefs] OFF
GO
SET IDENTITY_INSERT [dbo].[Dishes] ON 

INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (1, 12, N'Oshizushi', N'JapaneseFood-img\oshizushi-sushi.png', 1, N'Oshizushi (Pressed Sushi), or hakozushi (Boxed Sushi), features ingredients tightly packed into a box and layered with toppings. Originating more than 400 years ago in the Kansai region of Osaka, the packaging of the contents with fermented rice was another method of preservation.')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (2, 12, N'Chirashi Sushi', N'JapaneseFood-img\chirashi-sushi.png', 2, N'Chirashi, in Japanese, means “scattered,” and refers to a bowl of vinegar rice topped with whatever the cook wants to add')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (3, 12, N'Gunkanmaki Sushi', N'JapaneseFood-img\gunkanmaki-sushi.png', 3, N'Gunkanmaki is a type of maki roll, which is wrapped or rolled sushi. The sushi roll includes a wide strip of nori (edible seaweed) wrapped around a ball of rice. When this is made, space is left at the top to fill in with other ingredients.')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (4, 12, N'Kakinoha Sushi', N'JapaneseFood-img\kakinoha-zushi.png', 4, N'Kakinoha-zushi (Persimmon Leaf Sushi) originally comes from Nara prefecture, an important center of ancient Japanese culture. Nara is known for its pickling traditions (narazuke) and kakinoha’s ingredients come wrapped in salt-pickled persimmon leaves.')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (5, 12, N'Makizushi Sushi', N'JapaneseFood-img\maki-sushi.png', 5, N'Makizushi, also known as norimaki, is a type of sushi where the rice and other ingredients are rolled up in a sheet of nori (seaweed). Maki is usually cut into six to eight pieces and within the makizushi style, there are some additional options. ')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (6, 12, N'Temaki Sushi', N'JapaneseFood-img\temaki-sushi.png', 6, N'Temaki is like an ice cream cone or a Japanese taco with the ingredients spilling out over the sides of its seaweed cone.')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (7, 12, N'Uramaki Sushi', N'JapaneseFood-img\uramaki-sushi.png', 67, N'Uramaki (“inside-out”) sushi features rice on the outside wrapped around nori seaweed and other ingredients. Also known as California Roll, inside-out sushi originated in North America in the 1970s with several chefs claiming they created the style.')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (8, 12, N'Narezushi Sushi', N'JapaneseFood-img\narezushi-sushi.png', 7, N'The OG of sushi, Narezushi is an ancient form consisting of fermented fish preserved/pickled with salt and rice. Given that the dish is thousands of years old there are many variations of this dish in the historical record. It’s not totally clear when exactly narezushi began, but many people here consider this a family-style dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (9, 12, N'Nigiri Sushi', N'JapaneseFood-img\nigiri-sushi.png', 8, N'Nigiri, also known as edomae sushi, features vinegar rice molded by hand into a ball with a slice of sashimi (raw fish) placed on top. Considered to be a fast-food preparation, Hanaya Yobei, a sushi peddler in 19th century Tokyo, is credited with coming up with the sushi style.')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (10, 12, N'Sasazushi Sushi', N'JapaneseFood-img\sasazushi.png', 10, N'Sasazushi, from the Hokuriku region, is sushi wrapped in bamboo. During the 16th century and the Sengoku period of constant civil war, Japanese warlord Uesugi Kenshin, of the Samurai Nagao clan, fed his army sushi wrapped in bamboo, or so the story goes.')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (11, 12, N'Temari Sushi', N'JapaneseFood-img\temarizushi.png', 12, N'Temari are bite-sized sushi balls of rice with seafood and vegetable topping. Lady Murasaki, in her 11th century novel, The Tales of Genji, wrote of guests at a party eating egg-shaped rice balls. This type of sushi is reminiscent of onigiri (literally, “love and comfort), a traditional Japanese comfort food popular at picnics and parties. ')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (12, 8, N'Chawanmushi', N'JapaneseFood-img\chawanmushi.png', 24, N'Chawanmushi is a classic Japanese savory custard that’s steamed and served in a delicate cup. Tender chicken pieces, colorful kamaboko fish cake, and shimeji mushrooms are draped in a smooth and silky custard seasoned with dashi soup stock.')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (13, 1, N'Okonomiyaki', N'JapaneseFood-img\okonomiyaki.png', 30, N'Popular street food from Osaka, Japan, Okonomiyaki is a savory version of Japanese pancake, made with flour, eggs, shredded cabbage, and your choice of protein, and topped with a variety of condiments.')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (14, 13, N'Udon', N'JapaneseFood-img\udon.png', 29, N'Udon is a thick noodle made from wheat flour, used in Japanese cuisine. There are a variety of ways it is prepared and served. Its simplest form is in a hot soup as kake udon with a mild broth called kakejiru made from dashi, soy sauce, and mirin.')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (15, 13, N'Yakisoba', N'JapaneseFood-img\yakisoba.png', 11, N'Yakisoba is the Japanese version of stir-fried noodles. The noodles are cooked with sliced pork and plenty of vegetables, then coated with a special sauce. What distinguishes Yakisoba from other Asian stir-fried noodles is this special sauce, which is sweet and a little bit spicy.')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (16, 13, N'Ramen', N'JapaneseFood-img\ramen.png', 36, N'Ramen are thin, yellow noodles made from wheat and typically served in a flavorful hot broth. Kansui (alkaline water) gives ramen noodles their unique, springy texture and yellow color. Ramen originated in China, but it became very popular in Japan, particularly after World War II, when Ramen’s popularity rose above other Japanese noodles such as soba and udon.')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (17, 6, N'Ebi Sashimi', N'JapaneseFood-img\Ebi-sashimi.png', 48, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (18, 6, N'Akami Sashimi', N'JapaneseFood-img\Akami-sashimi.png', 50, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (19, 6, N'Ahi Sashimi', N'JapaneseFood-img\Ahi-sashimi.png', 52, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (20, 1, N'Gyoza', N'JapaneseFood-img\gyoza.png', 18, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (21, 1, N'Yakitori', N'JapaneseFood-img\yakitori.png', 20, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (22, 11, N'Oden', N'JapaneseFood-img\Ahi-sashimi.png', 56, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (23, 11, N'Shabu Shabu', N'JapaneseFood-img\shabu-shabu.png', 67, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (24, 11, N'Sukiyaki', N'JapaneseFood-img\sukiyaki.png', 78, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (25, 14, N'Curry Rice', N'JapaneseFood-img\curry-rice.png', 89, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (26, 14, N'Unagi Rice', N'JapaneseFood-img\unagi.png', 90, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (27, 11, N'Sukiyaki', N'JapaneseFood-img\sukiyaki.png', 23, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (28, 15, N'Bangohan', N'JapaneseFood-img\bangohan.png', 34, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (29, 15, N'Asagohan', N'JapaneseFood-img\asagohan.png', 45, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (30, 13, N'Soba', N'JapaneseFood-img\soba.png', 54, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (31, 12, N'Inari Sushi', N'JapaneseFood-img\inari-sushi.png', 43, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (32, 14, N'Tamagoyaki', N'JapaneseFood-img\tamagoyaki.png', 32, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (33, 14, N'Onigiri', N'JapaneseFood-img\onigiri.png', 87, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (34, 6, N'Suzuki Sashimi', N'JapaneseFood-img\Suzuki-sashimi.png', 98, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (35, 8, N'Dango', N'JapaneseFood-img\dango.png', 27, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (36, 8, N'Ichigo Daifuku', N'JapaneseFood-img\ichigodaifuku.png', 24, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (37, 8, N'Dango', N'JapaneseFood-img\dango.png', 37, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (38, 8, N'Sakura Daifuku', N'JapaneseFood-img\sakuradaifuku.png', 24, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (39, 8, N'Kasutera', N'JapaneseFood-img\kasutera.png', 24, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (40, 8, N'Taiyaki', N'JapaneseFood-img\taiyaki.png', 24, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (41, 8, N'Manju', N'JapaneseFood-img\manju.png', 24, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (42, 16, N'Ramune', N'JapaneseFood-img\ramune.png', 24, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (43, 16, N'Sake', N'JapaneseFood-img\sake.png', 24, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (44, 16, N'Asahi beer', N'JapaneseFood-img\asahibeer.png', 24, N'#This is description of this dish')
INSERT [dbo].[Dishes] ([Dish_ID], [Cat_ID], [Dish_Name], [Dish_Image], [Dish_Price], [Dish_Describe]) VALUES (45, 16, N'Sapporo beer', N'JapaneseFood-img\sapporo.png', 24, N'#This is description of this dish')
SET IDENTITY_INSERT [dbo].[Dishes] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderDetails] ON 

INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (1, 1, 1, 10)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (2, 2, 2, 11)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (3, 3, 3, 12)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (4, 4, 4, 13)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (5, 5, 5, 14)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (6, 6, 6, 15)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (7, 1, 1, 16)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (8, 2, 2, 17)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (9, 3, 3, 18)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (10, 4, 4, 19)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (11, 5, 5, 20)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (12, 6, 6, 21)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (13, 1, 1, 22)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (14, 2, 2, 23)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (15, 3, 3, 24)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (16, 4, 4, 25)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (17, 5, 5, 26)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (18, 6, 6, 27)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (19, 1, 1, 28)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (20, 2, 2, 29)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (21, 3, 3, 30)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (22, 4, 4, 31)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (23, 5, 5, 32)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (24, 6, 6, 33)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (25, 1, 1, 34)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (26, 2, 2, 35)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (27, 3, 3, 36)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (28, 4, 4, 37)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (29, 5, 5, 38)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (30, 6, 6, 39)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (31, 1, 1, 40)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (32, 1, 4, 37)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (33, 4, 2, 39)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (34, 3, 3, 38)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (35, 7, 7, 247)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (36, 8, 8, 124)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (37, 9, 9, 1)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (38, 10, 10, 1)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (39, 9, 11, 2)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (40, 8, 12, 13)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (41, 7, 13, 1)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (42, 6, 14, 15)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (43, 5, 15, 6)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (44, 4, 16, 7)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (45, 3, 17, 8)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (46, 7, 1, 10)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (47, 8, 2, 11)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (48, 9, 3, 12)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (49, 10, 4, 13)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (50, 11, 5, 14)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (51, 12, 6, 15)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (52, 13, 1, 16)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (53, 14, 2, 17)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (54, 15, 3, 18)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (55, 16, 4, 19)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (56, 17, 5, 20)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (57, 18, 6, 21)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (58, 19, 1, 22)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (59, 20, 2, 23)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (60, 21, 3, 24)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (61, 22, 4, 25)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (62, 23, 5, 26)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (63, 24, 6, 27)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (64, 25, 1, 28)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (65, 26, 2, 29)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (66, 27, 3, 30)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (67, 28, 4, 31)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (68, 29, 5, 32)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (69, 30, 6, 33)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (70, 31, 1, 34)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (71, 32, 2, 35)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (72, 33, 3, 36)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (73, 34, 4, 37)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (74, 35, 5, 38)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (75, 36, 6, 39)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (76, 31, 1, 10)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (77, 32, 2, 11)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (78, 33, 3, 12)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (79, 34, 4, 13)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (80, 35, 5, 14)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (81, 36, 6, 15)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (82, 31, 1, 16)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (83, 32, 2, 17)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (84, 33, 3, 18)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (85, 34, 4, 19)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (86, 35, 5, 20)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (87, 36, 6, 21)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (88, 41, 1, 22)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (89, 42, 2, 23)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (90, 43, 3, 24)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (91, 44, 4, 25)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (92, 45, 5, 26)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (93, 46, 6, 27)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (94, 41, 1, 28)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (95, 42, 2, 29)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (96, 43, 3, 30)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (97, 54, 4, 31)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (98, 55, 5, 32)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (99, 56, 6, 33)
GO
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (100, 51, 1, 34)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (101, 52, 2, 35)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (102, 53, 3, 36)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (103, 54, 4, 37)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (104, 65, 5, 38)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (105, 66, 6, 39)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (106, 61, 1, 40)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (107, 71, 4, 37)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (108, 74, 2, 39)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (109, 73, 3, 38)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (110, 77, 7, 247)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (111, 81, 8, 124)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (112, 89, 9, 1)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (113, 101, 10, 1)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (114, 90, 11, 2)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (115, 83, 12, 13)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (116, 79, 13, 1)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (117, 86, 14, 15)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (118, 85, 15, 6)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (119, 94, 16, 7)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (120, 93, 17, 8)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (121, 97, 1, 10)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (122, 88, 2, 11)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (123, 109, 3, 12)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (124, 110, 4, 13)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (125, 111, 5, 14)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (126, 92, 6, 15)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (127, 123, 1, 16)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (128, 84, 2, 17)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (129, 75, 3, 18)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (130, 96, 4, 19)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (131, 87, 5, 20)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (132, 98, 6, 21)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (133, 99, 1, 22)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (134, 100, 2, 23)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (135, 107, 3, 24)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (136, 72, 4, 25)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (137, 81, 5, 26)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (138, 56, 6, 27)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (139, 60, 1, 28)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (140, 70, 2, 29)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (141, 80, 3, 30)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (142, 71, 4, 31)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (143, 42, 5, 32)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (144, 30, 6, 33)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (145, 56, 1, 34)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (146, 63, 2, 35)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (147, 64, 3, 36)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (148, 69, 4, 37)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (149, 69, 5, 38)
INSERT [dbo].[OrderDetails] ([number], [Order_ID], [Dish_ID], [Quantity]) VALUES (150, 96, 6, 39)
SET IDENTITY_INSERT [dbo].[OrderDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (1, 1, CAST(N'2023-03-10' AS Date), CAST(N'17:29:00' AS Time), 1, N'Dom B', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (2, 1, CAST(N'2023-03-10' AS Date), CAST(N'17:30:00' AS Time), 1, N'Dom A', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (3, 2, CAST(N'2023-03-10' AS Date), CAST(N'17:31:00' AS Time), 1, N'Dom B', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (4, 3, CAST(N'2023-03-10' AS Date), CAST(N'17:32:00' AS Time), 1, N'Dom A', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (5, 4, CAST(N'2023-03-10' AS Date), CAST(N'17:33:00' AS Time), 1, N'Dom B', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (6, 5, CAST(N'2023-03-10' AS Date), CAST(N'17:34:00' AS Time), 1, N'Dom C', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (7, 6, CAST(N'2023-03-10' AS Date), CAST(N'17:35:00' AS Time), 1, N'Dom B', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (8, 1, CAST(N'2023-03-10' AS Date), CAST(N'17:36:00' AS Time), 1, N'Dom D', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (9, 2, CAST(N'2023-03-10' AS Date), CAST(N'17:37:00' AS Time), 1, N'Dom E', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (10, 3, CAST(N'2023-03-10' AS Date), CAST(N'17:38:00' AS Time), 1, N'Dom F', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (11, 4, CAST(N'2023-03-10' AS Date), CAST(N'17:39:00' AS Time), 1, N'Dom G', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (12, 5, CAST(N'2023-03-10' AS Date), CAST(N'17:40:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (13, 6, CAST(N'2023-03-10' AS Date), CAST(N'17:41:00' AS Time), 1, N'Dom A', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (14, 1, CAST(N'2023-03-11' AS Date), CAST(N'17:42:00' AS Time), 1, N'Dom A', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (15, 2, CAST(N'2023-03-11' AS Date), CAST(N'17:43:00' AS Time), 1, N'Dom C', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (16, 3, CAST(N'2023-03-11' AS Date), CAST(N'17:44:00' AS Time), 1, N'Dom B', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (17, 4, CAST(N'2023-03-11' AS Date), CAST(N'17:45:00' AS Time), 1, N'Dom D', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (18, 5, CAST(N'2023-03-11' AS Date), CAST(N'17:46:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (19, 6, CAST(N'2023-03-11' AS Date), CAST(N'17:47:00' AS Time), 1, N'Dom F', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (20, 1, CAST(N'2023-03-11' AS Date), CAST(N'17:48:00' AS Time), 1, N'Dom G', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (21, 2, CAST(N'2023-03-11' AS Date), CAST(N'17:49:00' AS Time), 1, N'Dom E', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (22, 3, CAST(N'2023-03-11' AS Date), CAST(N'17:50:00' AS Time), 1, N'Dom H', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (23, 4, CAST(N'2023-03-11' AS Date), CAST(N'17:51:00' AS Time), 1, N'Dom B', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (24, 5, CAST(N'2023-03-11' AS Date), CAST(N'17:52:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (25, 6, CAST(N'2023-03-11' AS Date), CAST(N'17:53:00' AS Time), 1, N'Dom B', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (26, 1, CAST(N'2023-03-12' AS Date), CAST(N'17:54:00' AS Time), 1, N'Dom B', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (27, 2, CAST(N'2023-03-12' AS Date), CAST(N'17:55:00' AS Time), 1, N'Dom B', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (28, 3, CAST(N'2023-03-12' AS Date), CAST(N'17:56:00' AS Time), 1, N'Dom B', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (29, 4, CAST(N'2023-03-12' AS Date), CAST(N'17:57:00' AS Time), 1, N'Dom B', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (30, 5, CAST(N'2023-03-12' AS Date), CAST(N'17:58:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (31, 6, CAST(N'2023-03-12' AS Date), CAST(N'17:59:00' AS Time), 1, N'Dom B', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (32, 1, CAST(N'2023-03-12' AS Date), CAST(N'17:29:00' AS Time), 1, N'Dom B', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (33, 1, CAST(N'2023-03-12' AS Date), CAST(N'17:30:00' AS Time), 1, N'Dom A', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (34, 2, CAST(N'2023-03-12' AS Date), CAST(N'17:31:00' AS Time), 1, N'Dom B', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (35, 3, CAST(N'2023-03-12' AS Date), CAST(N'17:32:00' AS Time), 1, N'Dom A', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (36, 4, CAST(N'2023-03-12' AS Date), CAST(N'17:33:00' AS Time), 1, N'Dom B', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (37, 5, CAST(N'2023-03-12' AS Date), CAST(N'17:34:00' AS Time), 1, N'Dom C', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (38, 6, CAST(N'2023-03-12' AS Date), CAST(N'17:35:00' AS Time), 1, N'Dom B', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (39, 1, CAST(N'2023-03-13' AS Date), CAST(N'17:36:00' AS Time), 1, N'Dom D', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (40, 2, CAST(N'2023-03-13' AS Date), CAST(N'17:37:00' AS Time), 1, N'Dom E', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (41, 3, CAST(N'2023-03-13' AS Date), CAST(N'17:38:00' AS Time), 1, N'Dom F', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (42, 4, CAST(N'2023-03-13' AS Date), CAST(N'17:39:00' AS Time), 1, N'Dom G', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (43, 5, CAST(N'2023-03-13' AS Date), CAST(N'17:40:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (44, 6, CAST(N'2023-03-13' AS Date), CAST(N'17:41:00' AS Time), 1, N'Dom A', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (45, 1, CAST(N'2023-03-13' AS Date), CAST(N'17:42:00' AS Time), 1, N'Dom A', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (46, 2, CAST(N'2023-03-13' AS Date), CAST(N'17:43:00' AS Time), 1, N'Dom C', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (47, 3, CAST(N'2023-03-13' AS Date), CAST(N'17:44:00' AS Time), 1, N'Dom B', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (48, 4, CAST(N'2023-03-13' AS Date), CAST(N'17:45:00' AS Time), 1, N'Dom D', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (49, 5, CAST(N'2023-03-13' AS Date), CAST(N'17:46:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (50, 6, CAST(N'2023-03-13' AS Date), CAST(N'17:47:00' AS Time), 1, N'Dom F', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (51, 1, CAST(N'2023-03-14' AS Date), CAST(N'17:48:00' AS Time), 1, N'Dom G', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (52, 2, CAST(N'2023-03-14' AS Date), CAST(N'17:49:00' AS Time), 1, N'Dom E', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (53, 3, CAST(N'2023-03-14' AS Date), CAST(N'17:50:00' AS Time), 1, N'Dom H', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (54, 4, CAST(N'2023-03-14' AS Date), CAST(N'17:51:00' AS Time), 1, N'Dom B', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (55, 5, CAST(N'2023-03-14' AS Date), CAST(N'17:52:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (56, 6, CAST(N'2023-03-14' AS Date), CAST(N'17:53:00' AS Time), 1, N'Dom B', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (57, 1, CAST(N'2023-03-14' AS Date), CAST(N'17:54:00' AS Time), 1, N'Dom B', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (58, 2, CAST(N'2023-03-14' AS Date), CAST(N'17:55:00' AS Time), 1, N'Dom B', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (59, 3, CAST(N'2023-03-14' AS Date), CAST(N'17:56:00' AS Time), 1, N'Dom B', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (60, 4, CAST(N'2023-03-14' AS Date), CAST(N'17:57:00' AS Time), 1, N'Dom B', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (61, 5, CAST(N'2023-03-14' AS Date), CAST(N'17:58:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (62, 6, CAST(N'2023-03-14' AS Date), CAST(N'17:59:00' AS Time), 1, N'Dom B', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (63, 1, CAST(N'2023-02-10' AS Date), CAST(N'17:29:00' AS Time), 1, N'Dom B', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (64, 1, CAST(N'2023-02-10' AS Date), CAST(N'17:30:00' AS Time), 1, N'Dom A', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (65, 2, CAST(N'2023-02-10' AS Date), CAST(N'17:31:00' AS Time), 1, N'Dom B', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (66, 3, CAST(N'2023-02-10' AS Date), CAST(N'17:32:00' AS Time), 1, N'Dom A', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (67, 4, CAST(N'2023-02-10' AS Date), CAST(N'17:33:00' AS Time), 1, N'Dom B', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (68, 5, CAST(N'2023-02-10' AS Date), CAST(N'17:34:00' AS Time), 1, N'Dom C', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (69, 6, CAST(N'2023-02-10' AS Date), CAST(N'17:35:00' AS Time), 1, N'Dom B', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (70, 1, CAST(N'2023-02-10' AS Date), CAST(N'17:36:00' AS Time), 1, N'Dom D', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (71, 2, CAST(N'2023-02-10' AS Date), CAST(N'17:37:00' AS Time), 1, N'Dom E', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (72, 3, CAST(N'2023-02-10' AS Date), CAST(N'17:38:00' AS Time), 1, N'Dom F', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (73, 4, CAST(N'2023-02-10' AS Date), CAST(N'17:39:00' AS Time), 1, N'Dom G', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (74, 5, CAST(N'2023-02-10' AS Date), CAST(N'17:40:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (75, 6, CAST(N'2023-02-10' AS Date), CAST(N'17:41:00' AS Time), 1, N'Dom A', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (76, 1, CAST(N'2023-02-11' AS Date), CAST(N'17:42:00' AS Time), 1, N'Dom A', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (77, 2, CAST(N'2023-02-11' AS Date), CAST(N'17:43:00' AS Time), 1, N'Dom C', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (78, 3, CAST(N'2023-02-11' AS Date), CAST(N'17:44:00' AS Time), 1, N'Dom B', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (79, 4, CAST(N'2023-02-11' AS Date), CAST(N'17:45:00' AS Time), 1, N'Dom D', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (80, 5, CAST(N'2023-02-11' AS Date), CAST(N'17:46:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (81, 6, CAST(N'2023-02-11' AS Date), CAST(N'17:47:00' AS Time), 1, N'Dom F', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (82, 1, CAST(N'2023-02-11' AS Date), CAST(N'17:48:00' AS Time), 1, N'Dom G', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (83, 2, CAST(N'2023-02-11' AS Date), CAST(N'17:49:00' AS Time), 1, N'Dom E', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (84, 3, CAST(N'2023-02-11' AS Date), CAST(N'17:50:00' AS Time), 1, N'Dom H', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (85, 4, CAST(N'2023-02-11' AS Date), CAST(N'17:51:00' AS Time), 1, N'Dom B', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (86, 5, CAST(N'2023-02-11' AS Date), CAST(N'17:52:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (87, 6, CAST(N'2023-02-11' AS Date), CAST(N'17:53:00' AS Time), 1, N'Dom B', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (88, 1, CAST(N'2023-02-12' AS Date), CAST(N'17:54:00' AS Time), 1, N'Dom B', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (89, 2, CAST(N'2023-02-12' AS Date), CAST(N'17:55:00' AS Time), 1, N'Dom B', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (90, 3, CAST(N'2023-02-12' AS Date), CAST(N'17:56:00' AS Time), 1, N'Dom B', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (91, 4, CAST(N'2023-02-12' AS Date), CAST(N'17:57:00' AS Time), 1, N'Dom B', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (92, 5, CAST(N'2023-02-12' AS Date), CAST(N'17:58:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (93, 6, CAST(N'2023-02-12' AS Date), CAST(N'17:59:00' AS Time), 1, N'Dom B', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (94, 1, CAST(N'2023-02-12' AS Date), CAST(N'17:29:00' AS Time), 1, N'Dom B', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (95, 1, CAST(N'2023-02-12' AS Date), CAST(N'17:30:00' AS Time), 1, N'Dom A', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (96, 2, CAST(N'2023-02-12' AS Date), CAST(N'17:31:00' AS Time), 1, N'Dom B', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (97, 3, CAST(N'2023-02-12' AS Date), CAST(N'17:32:00' AS Time), 1, N'Dom A', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (98, 4, CAST(N'2023-02-12' AS Date), CAST(N'17:33:00' AS Time), 1, N'Dom B', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (99, 5, CAST(N'2023-02-12' AS Date), CAST(N'17:34:00' AS Time), 1, N'Dom C', 5, NULL)
GO
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (100, 6, CAST(N'2023-02-12' AS Date), CAST(N'17:35:00' AS Time), 1, N'Dom B', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (101, 1, CAST(N'2023-02-13' AS Date), CAST(N'17:36:00' AS Time), 1, N'Dom D', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (102, 2, CAST(N'2023-02-13' AS Date), CAST(N'17:37:00' AS Time), 1, N'Dom E', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (103, 3, CAST(N'2023-02-13' AS Date), CAST(N'17:38:00' AS Time), 1, N'Dom F', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (104, 4, CAST(N'2023-02-13' AS Date), CAST(N'17:39:00' AS Time), 1, N'Dom G', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (105, 5, CAST(N'2023-02-13' AS Date), CAST(N'17:40:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (106, 6, CAST(N'2023-02-13' AS Date), CAST(N'17:41:00' AS Time), 1, N'Dom A', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (107, 1, CAST(N'2023-02-13' AS Date), CAST(N'17:42:00' AS Time), 1, N'Dom A', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (108, 2, CAST(N'2023-02-13' AS Date), CAST(N'17:43:00' AS Time), 1, N'Dom C', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (109, 3, CAST(N'2023-02-13' AS Date), CAST(N'17:44:00' AS Time), 1, N'Dom B', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (110, 4, CAST(N'2023-02-13' AS Date), CAST(N'17:45:00' AS Time), 1, N'Dom D', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (111, 5, CAST(N'2023-02-13' AS Date), CAST(N'17:46:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (112, 6, CAST(N'2023-02-13' AS Date), CAST(N'17:47:00' AS Time), 1, N'Dom F', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (113, 1, CAST(N'2023-02-14' AS Date), CAST(N'17:48:00' AS Time), 1, N'Dom G', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (114, 2, CAST(N'2023-02-14' AS Date), CAST(N'17:49:00' AS Time), 1, N'Dom E', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (115, 3, CAST(N'2023-02-14' AS Date), CAST(N'17:50:00' AS Time), 1, N'Dom H', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (116, 4, CAST(N'2023-02-14' AS Date), CAST(N'17:51:00' AS Time), 1, N'Dom B', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (117, 5, CAST(N'2023-02-14' AS Date), CAST(N'17:52:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (118, 6, CAST(N'2023-02-14' AS Date), CAST(N'17:53:00' AS Time), 1, N'Dom B', 6, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (119, 1, CAST(N'2023-02-14' AS Date), CAST(N'17:54:00' AS Time), 1, N'Dom B', 1, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (120, 2, CAST(N'2023-02-14' AS Date), CAST(N'17:55:00' AS Time), 1, N'Dom B', 2, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (121, 3, CAST(N'2023-02-14' AS Date), CAST(N'17:56:00' AS Time), 1, N'Dom B', 3, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (122, 4, CAST(N'2023-02-14' AS Date), CAST(N'17:57:00' AS Time), 1, N'Dom B', 4, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (123, 5, CAST(N'2023-02-14' AS Date), CAST(N'17:58:00' AS Time), 1, N'Dom B', 5, NULL)
INSERT [dbo].[Orders] ([Order_ID], [U_ID], [OrderDate], [OrderTime], [Shipped], [ShipAddress], [Shipper_ID], [Total_money]) VALUES (124, 6, CAST(N'2023-02-14' AS Date), CAST(N'17:59:00' AS Time), 1, N'Dom B', 6, NULL)
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[Shippers] ON 

INSERT [dbo].[Shippers] ([Shipper_ID], [Shipper_Name], [Shipper_Phone], [Shipper_Img]) VALUES (1, N'Pham Duc Trung', N'0123456701', N'JapaneseFoodStaff-img\shipimg1.png')
INSERT [dbo].[Shippers] ([Shipper_ID], [Shipper_Name], [Shipper_Phone], [Shipper_Img]) VALUES (2, N'Nguyen Linh Anh', N'0123456702', N'JapaneseFoodStaff-img\shipimg2.png')
INSERT [dbo].[Shippers] ([Shipper_ID], [Shipper_Name], [Shipper_Phone], [Shipper_Img]) VALUES (3, N'Tran Anh Duc', N'0123456703', N'JapaneseFoodStaff-img\shipimg3.png')
INSERT [dbo].[Shippers] ([Shipper_ID], [Shipper_Name], [Shipper_Phone], [Shipper_Img]) VALUES (4, N'Cao Van Tai', N'0123456704', N'JapaneseFoodStaff-img\shipimg4.png')
INSERT [dbo].[Shippers] ([Shipper_ID], [Shipper_Name], [Shipper_Phone], [Shipper_Img]) VALUES (5, N'Nhat Duong', N'0123456705', N'JapaneseFoodStaff-img\shipimg5.png')
INSERT [dbo].[Shippers] ([Shipper_ID], [Shipper_Name], [Shipper_Phone], [Shipper_Img]) VALUES (6, N'Tong Quang Khai', N'0123456706', N'JapaneseFoodStaff-img\shipimg6.png')
SET IDENTITY_INSERT [dbo].[Shippers] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([U_ID], [username], [password], [U_FirstName], [U_LastName], [U_Address], [U_Phone], [U_Email], [IsAdmin]) VALUES (1, N'sa', N'123', N'sa', N'sa', N'VietNam', N'0123000000', N'sa@gmail.com', 1)
INSERT [dbo].[Users] ([U_ID], [username], [password], [U_FirstName], [U_LastName], [U_Address], [U_Phone], [U_Email], [IsAdmin]) VALUES (2, N'naviank', N'240702', N'Van', N'Pham', N'Bac Ninh', N'0384478185', N'cbnlolivan@gmail.com', 1)
INSERT [dbo].[Users] ([U_ID], [username], [password], [U_FirstName], [U_LastName], [U_Address], [U_Phone], [U_Email], [IsAdmin]) VALUES (3, N'jocasta', N'291104', N'Linh', N'Nguyen', N'Yen Phong', N'0866798281', N'sa@gmail.com', 1)
INSERT [dbo].[Users] ([U_ID], [username], [password], [U_FirstName], [U_LastName], [U_Address], [U_Phone], [U_Email], [IsAdmin]) VALUES (4, N'htrangg', N'280502', N'Trang', N'Ha', N'Tien Du', N'0968175246', N'htrg@gmail.com', 1)
INSERT [dbo].[Users] ([U_ID], [username], [password], [U_FirstName], [U_LastName], [U_Address], [U_Phone], [U_Email], [IsAdmin]) VALUES (5, N'n3t', N'120402', N'Trang', N'Thu', N'Tien Du', N'0353040830', N'n3t@gmail.com', 1)
INSERT [dbo].[Users] ([U_ID], [username], [password], [U_FirstName], [U_LastName], [U_Address], [U_Phone], [U_Email], [IsAdmin]) VALUES (6, N'dtl', N'081203', N'Linh', N'Do', N'Tien Du', N'0379612097', N'dtl@gmail.com', 1)
INSERT [dbo].[Users] ([U_ID], [username], [password], [U_FirstName], [U_LastName], [U_Address], [U_Phone], [U_Email], [IsAdmin]) VALUES (7, N'ltd', N'290703', N'Dung', N'Le', N'Yen Phong', N'0326885925', N'ltd@gmail.com', 1)
INSERT [dbo].[Users] ([U_ID], [username], [password], [U_FirstName], [U_LastName], [U_Address], [U_Phone], [U_Email], [IsAdmin]) VALUES (8, N'pkv', N'123', N'Van', N'Pham Khai', N'Hoa Lac', N'0384478185', N'vanpham@gmail.com', 0)
INSERT [dbo].[Users] ([U_ID], [username], [password], [U_FirstName], [U_LastName], [U_Address], [U_Phone], [U_Email], [IsAdmin]) VALUES (9, N'pkv', N'123', N'Van', N'Pham Khai', N'Hoa Lac', N'0384478185', N'vanpham@gmail.com', 0)
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD FOREIGN KEY([Chef_ID])
REFERENCES [dbo].[Chefs] ([Chef_ID])
GO
ALTER TABLE [dbo].[Dishes]  WITH CHECK ADD FOREIGN KEY([Cat_ID])
REFERENCES [dbo].[Categories] ([Cat_ID])
GO
ALTER TABLE [dbo].[OrderDetails]  WITH CHECK ADD  CONSTRAINT [fk_Detail_Dishes] FOREIGN KEY([Dish_ID])
REFERENCES [dbo].[Dishes] ([Dish_ID])
GO
ALTER TABLE [dbo].[OrderDetails] CHECK CONSTRAINT [fk_Detail_Dishes]
GO
ALTER TABLE [dbo].[OrderDetails]  WITH CHECK ADD  CONSTRAINT [fk_Detail_Orders] FOREIGN KEY([Order_ID])
REFERENCES [dbo].[Orders] ([Order_ID])
GO
ALTER TABLE [dbo].[OrderDetails] CHECK CONSTRAINT [fk_Detail_Orders]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([Shipper_ID])
REFERENCES [dbo].[Shippers] ([Shipper_ID])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([U_ID])
REFERENCES [dbo].[Users] ([U_ID])
GO
USE [master]
GO
ALTER DATABASE [JapaneseRestaurant] SET  READ_WRITE 
GO
